//------------------------------------------------------------------------------
// <auto-generated>
//     Этот код создан по шаблону.
//
//     Изменения, вносимые в этот файл вручную, могут привести к непредвиденной работе приложения.
//     Изменения, вносимые в этот файл вручную, будут перезаписаны при повторном создании кода.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ParticipationInOlympiads
{
    using System;
    using System.Collections.Generic;
    
    public partial class SchoolStageTable
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public SchoolStageTable()
        {
            this.OlympiadByAgeTables = new HashSet<OlympiadByAgeTable>();
        }
    
        public int SchoolStageId { get; set; }
        public string SchoolStage { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OlympiadByAgeTable> OlympiadByAgeTables { get; set; }
    }
}
