﻿
namespace ParticipationInOlympiads
{
    partial class FormNewParticipant
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormNewParticipant));
            this.cbTeam = new System.Windows.Forms.CheckBox();
            this.lblLevel = new System.Windows.Forms.Label();
            this.cbSchoolStage = new System.Windows.Forms.ComboBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.lblSchoolStage = new System.Windows.Forms.Label();
            this.lblName = new System.Windows.Forms.Label();
            this.cbNameOlympiad = new System.Windows.Forms.ComboBox();
            this.cbLevel = new System.Windows.Forms.ComboBox();
            this.lblDate = new System.Windows.Forms.Label();
            this.dtpActualDate = new System.Windows.Forms.DateTimePicker();
            this.cbStudent = new System.Windows.Forms.ComboBox();
            this.lblStudent = new System.Windows.Forms.Label();
            this.lblSScores = new System.Windows.Forms.Label();
            this.tbScores = new System.Windows.Forms.TextBox();
            this.cbResult = new System.Windows.Forms.ComboBox();
            this.cbReward = new System.Windows.Forms.ComboBox();
            this.lblReward = new System.Windows.Forms.Label();
            this.lblResult = new System.Windows.Forms.Label();
            this.lblClass = new System.Windows.Forms.Label();
            this.tbClass = new System.Windows.Forms.TextBox();
            this.btnEditInfoParticipant = new System.Windows.Forms.Button();
            this.pnlRegistration = new System.Windows.Forms.Panel();
            this.cbAbsent = new System.Windows.Forms.CheckBox();
            this.btnFurther = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.pnlRegistration.SuspendLayout();
            this.SuspendLayout();
            // 
            // cbTeam
            // 
            this.cbTeam.AutoSize = true;
            this.cbTeam.Enabled = false;
            this.cbTeam.Location = new System.Drawing.Point(136, 135);
            this.cbTeam.Name = "cbTeam";
            this.cbTeam.Size = new System.Drawing.Size(143, 17);
            this.cbTeam.TabIndex = 37;
            this.cbTeam.Text = "Олимпиада командная";
            this.cbTeam.UseVisualStyleBackColor = true;
            // 
            // lblLevel
            // 
            this.lblLevel.AutoSize = true;
            this.lblLevel.Location = new System.Drawing.Point(56, 100);
            this.lblLevel.Name = "lblLevel";
            this.lblLevel.Size = new System.Drawing.Size(54, 13);
            this.lblLevel.TabIndex = 33;
            this.lblLevel.Text = "Уровень:";
            // 
            // cbSchoolStage
            // 
            this.cbSchoolStage.BackColor = System.Drawing.Color.White;
            this.cbSchoolStage.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbSchoolStage.FormattingEnabled = true;
            this.cbSchoolStage.Location = new System.Drawing.Point(136, 59);
            this.cbSchoolStage.Name = "cbSchoolStage";
            this.cbSchoolStage.Size = new System.Drawing.Size(318, 21);
            this.cbSchoolStage.TabIndex = 2;
            this.cbSchoolStage.SelectedIndexChanged += new System.EventHandler(this.cbSchoolStage_SelectedIndexChanged);
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btnSave.FlatAppearance.BorderSize = 0;
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSave.Location = new System.Drawing.Point(194, 412);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 8;
            this.btnSave.Text = "Сохранить";
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // lblSchoolStage
            // 
            this.lblSchoolStage.AutoSize = true;
            this.lblSchoolStage.Location = new System.Drawing.Point(12, 62);
            this.lblSchoolStage.Name = "lblSchoolStage";
            this.lblSchoolStage.Size = new System.Drawing.Size(98, 13);
            this.lblSchoolStage.TabIndex = 30;
            this.lblSchoolStage.Text = "Область классов:";
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(42, 29);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(68, 13);
            this.lblName.TabIndex = 0;
            this.lblName.Text = "Олимпиада:";
            // 
            // cbNameOlympiad
            // 
            this.cbNameOlympiad.BackColor = System.Drawing.Color.White;
            this.cbNameOlympiad.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbNameOlympiad.FormattingEnabled = true;
            this.cbNameOlympiad.Location = new System.Drawing.Point(136, 26);
            this.cbNameOlympiad.Name = "cbNameOlympiad";
            this.cbNameOlympiad.Size = new System.Drawing.Size(318, 21);
            this.cbNameOlympiad.TabIndex = 1;
            this.cbNameOlympiad.SelectedIndexChanged += new System.EventHandler(this.cbNameOlympiad_SelectedIndexChanged);
            // 
            // cbLevel
            // 
            this.cbLevel.BackColor = System.Drawing.Color.White;
            this.cbLevel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbLevel.FormattingEnabled = true;
            this.cbLevel.Location = new System.Drawing.Point(136, 97);
            this.cbLevel.Name = "cbLevel";
            this.cbLevel.Size = new System.Drawing.Size(318, 21);
            this.cbLevel.TabIndex = 3;
            this.cbLevel.SelectedIndexChanged += new System.EventHandler(this.cbLevel_SelectedIndexChanged);
            // 
            // lblDate
            // 
            this.lblDate.AutoSize = true;
            this.lblDate.Location = new System.Drawing.Point(74, 174);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(36, 13);
            this.lblDate.TabIndex = 40;
            this.lblDate.Text = "Дата:";
            // 
            // dtpActualDate
            // 
            this.dtpActualDate.CustomFormat = "\"yyyy-dd-MM\"";
            this.dtpActualDate.Enabled = false;
            this.dtpActualDate.Location = new System.Drawing.Point(136, 168);
            this.dtpActualDate.MinDate = new System.DateTime(2019, 1, 1, 0, 0, 0, 0);
            this.dtpActualDate.Name = "dtpActualDate";
            this.dtpActualDate.Size = new System.Drawing.Size(318, 20);
            this.dtpActualDate.TabIndex = 38;
            this.dtpActualDate.Value = new System.DateTime(2021, 4, 21, 8, 53, 41, 0);
            // 
            // cbStudent
            // 
            this.cbStudent.BackColor = System.Drawing.Color.White;
            this.cbStudent.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawVariable;
            this.cbStudent.ForeColor = System.Drawing.SystemColors.WindowText;
            this.cbStudent.FormattingEnabled = true;
            this.cbStudent.Location = new System.Drawing.Point(136, 208);
            this.cbStudent.MaxDropDownItems = 12;
            this.cbStudent.Name = "cbStudent";
            this.cbStudent.Size = new System.Drawing.Size(257, 21);
            this.cbStudent.TabIndex = 4;
            this.cbStudent.DrawItem += new System.Windows.Forms.DrawItemEventHandler(this.cbStudent_DrawItem);
            this.cbStudent.SelectedIndexChanged += new System.EventHandler(this.cbStudent_SelectedIndexChanged);
            this.cbStudent.TextChanged += new System.EventHandler(this.cbStudent_TextChanged);
            // 
            // lblStudent
            // 
            this.lblStudent.AutoSize = true;
            this.lblStudent.Location = new System.Drawing.Point(63, 211);
            this.lblStudent.Name = "lblStudent";
            this.lblStudent.Size = new System.Drawing.Size(47, 13);
            this.lblStudent.TabIndex = 41;
            this.lblStudent.Text = "Ученик:";
            // 
            // lblSScores
            // 
            this.lblSScores.AutoSize = true;
            this.lblSScores.Location = new System.Drawing.Point(55, 31);
            this.lblSScores.Name = "lblSScores";
            this.lblSScores.Size = new System.Drawing.Size(43, 13);
            this.lblSScores.TabIndex = 44;
            this.lblSScores.Text = "Баллы:";
            // 
            // tbScores
            // 
            this.tbScores.Location = new System.Drawing.Point(124, 28);
            this.tbScores.Name = "tbScores";
            this.tbScores.Size = new System.Drawing.Size(78, 20);
            this.tbScores.TabIndex = 5;
            this.tbScores.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbScores_KeyPress);
            this.tbScores.Leave += new System.EventHandler(this.tbScores_Leave);
            // 
            // cbResult
            // 
            this.cbResult.BackColor = System.Drawing.Color.White;
            this.cbResult.FormattingEnabled = true;
            this.cbResult.Location = new System.Drawing.Point(124, 65);
            this.cbResult.Name = "cbResult";
            this.cbResult.Size = new System.Drawing.Size(318, 21);
            this.cbResult.TabIndex = 6;
            // 
            // cbReward
            // 
            this.cbReward.BackColor = System.Drawing.Color.White;
            this.cbReward.FormattingEnabled = true;
            this.cbReward.Location = new System.Drawing.Point(124, 102);
            this.cbReward.Name = "cbReward";
            this.cbReward.Size = new System.Drawing.Size(318, 21);
            this.cbReward.TabIndex = 7;
            // 
            // lblReward
            // 
            this.lblReward.AutoSize = true;
            this.lblReward.Location = new System.Drawing.Point(2, 105);
            this.lblReward.Name = "lblReward";
            this.lblReward.Size = new System.Drawing.Size(96, 13);
            this.lblReward.TabIndex = 47;
            this.lblReward.Text = "Вознаграждение:";
            // 
            // lblResult
            // 
            this.lblResult.AutoSize = true;
            this.lblResult.Location = new System.Drawing.Point(36, 68);
            this.lblResult.Name = "lblResult";
            this.lblResult.Size = new System.Drawing.Size(62, 13);
            this.lblResult.TabIndex = 46;
            this.lblResult.Text = "Результат:";
            // 
            // lblClass
            // 
            this.lblClass.AutoSize = true;
            this.lblClass.Location = new System.Drawing.Point(63, 248);
            this.lblClass.Name = "lblClass";
            this.lblClass.Size = new System.Drawing.Size(41, 13);
            this.lblClass.TabIndex = 50;
            this.lblClass.Text = "Класс:";
            // 
            // tbClass
            // 
            this.tbClass.Location = new System.Drawing.Point(136, 244);
            this.tbClass.Name = "tbClass";
            this.tbClass.ReadOnly = true;
            this.tbClass.Size = new System.Drawing.Size(78, 20);
            this.tbClass.TabIndex = 52;
            this.tbClass.TabStop = false;
            // 
            // btnEditInfoParticipant
            // 
            this.btnEditInfoParticipant.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btnEditInfoParticipant.FlatAppearance.BorderSize = 0;
            this.btnEditInfoParticipant.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEditInfoParticipant.Location = new System.Drawing.Point(230, 246);
            this.btnEditInfoParticipant.Name = "btnEditInfoParticipant";
            this.btnEditInfoParticipant.Size = new System.Drawing.Size(224, 23);
            this.btnEditInfoParticipant.TabIndex = 9;
            this.btnEditInfoParticipant.Text = "Изменить информацию об участнике";
            this.btnEditInfoParticipant.UseVisualStyleBackColor = false;
            this.btnEditInfoParticipant.Click += new System.EventHandler(this.btnEditInfoParticipant_Click);
            // 
            // pnlRegistration
            // 
            this.pnlRegistration.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.pnlRegistration.Controls.Add(this.cbAbsent);
            this.pnlRegistration.Controls.Add(this.lblSScores);
            this.pnlRegistration.Controls.Add(this.tbScores);
            this.pnlRegistration.Controls.Add(this.lblResult);
            this.pnlRegistration.Controls.Add(this.lblReward);
            this.pnlRegistration.Controls.Add(this.cbResult);
            this.pnlRegistration.Controls.Add(this.cbReward);
            this.pnlRegistration.Location = new System.Drawing.Point(12, 275);
            this.pnlRegistration.Name = "pnlRegistration";
            this.pnlRegistration.Size = new System.Drawing.Size(455, 131);
            this.pnlRegistration.TabIndex = 54;
            // 
            // cbAbsent
            // 
            this.cbAbsent.AutoSize = true;
            this.cbAbsent.Location = new System.Drawing.Point(5, 5);
            this.cbAbsent.Name = "cbAbsent";
            this.cbAbsent.Size = new System.Drawing.Size(134, 17);
            this.cbAbsent.TabIndex = 48;
            this.cbAbsent.Text = "Ученик отсутствовал";
            this.cbAbsent.UseVisualStyleBackColor = true;
            this.cbAbsent.CheckedChanged += new System.EventHandler(this.cbAbsent_CheckedChanged);
            // 
            // btnFurther
            // 
            this.btnFurther.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btnFurther.FlatAppearance.BorderSize = 0;
            this.btnFurther.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFurther.Location = new System.Drawing.Point(340, 412);
            this.btnFurther.Name = "btnFurther";
            this.btnFurther.Size = new System.Drawing.Size(75, 23);
            this.btnFurther.TabIndex = 55;
            this.btnFurther.Text = "Далее";
            this.btnFurther.UseVisualStyleBackColor = false;
            this.btnFurther.Click += new System.EventHandler(this.btnFurther_Click);
            // 
            // btnExit
            // 
            this.btnExit.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btnExit.FlatAppearance.BorderSize = 0;
            this.btnExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExit.Location = new System.Drawing.Point(59, 412);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(75, 23);
            this.btnExit.TabIndex = 56;
            this.btnExit.Text = "Выйти";
            this.btnExit.UseVisualStyleBackColor = false;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // FormNewParticipant
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.ClientSize = new System.Drawing.Size(486, 458);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.btnFurther);
            this.Controls.Add(this.pnlRegistration);
            this.Controls.Add(this.btnEditInfoParticipant);
            this.Controls.Add(this.tbClass);
            this.Controls.Add(this.lblClass);
            this.Controls.Add(this.cbStudent);
            this.Controls.Add(this.lblStudent);
            this.Controls.Add(this.lblDate);
            this.Controls.Add(this.dtpActualDate);
            this.Controls.Add(this.cbLevel);
            this.Controls.Add(this.cbNameOlympiad);
            this.Controls.Add(this.cbTeam);
            this.Controls.Add(this.lblLevel);
            this.Controls.Add(this.cbSchoolStage);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.lblSchoolStage);
            this.Controls.Add(this.lblName);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "FormNewParticipant";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Регистрация участника";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormNewParticipant_FormClosing);
            this.Load += new System.EventHandler(this.FormNewParticipant_Load);
            this.pnlRegistration.ResumeLayout(false);
            this.pnlRegistration.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.CheckBox cbTeam;
        private System.Windows.Forms.Label lblLevel;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Label lblSchoolStage;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.Label lblDate;
        private System.Windows.Forms.Label lblStudent;
        private System.Windows.Forms.Label lblSScores;
        private System.Windows.Forms.TextBox tbScores;
        private System.Windows.Forms.ComboBox cbResult;
        private System.Windows.Forms.ComboBox cbReward;
        private System.Windows.Forms.Label lblReward;
        private System.Windows.Forms.Label lblResult;
        private System.Windows.Forms.Label lblClass;
        private System.Windows.Forms.TextBox tbClass;
        private System.Windows.Forms.Button btnEditInfoParticipant;
        public System.Windows.Forms.Panel pnlRegistration;
        public System.Windows.Forms.DateTimePicker dtpActualDate;
        private System.Windows.Forms.Button btnFurther;
        private System.Windows.Forms.Button btnExit;
        public System.Windows.Forms.ComboBox cbSchoolStage;
        public System.Windows.Forms.ComboBox cbNameOlympiad;
        public System.Windows.Forms.ComboBox cbLevel;
        public System.Windows.Forms.ComboBox cbStudent;
        private System.Windows.Forms.CheckBox cbAbsent;
    }
}