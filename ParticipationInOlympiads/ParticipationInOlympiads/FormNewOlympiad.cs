﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParticipationInOlympiads
{
    public partial class FormNewOlympiad : Form
    {
        public FormNewOlympiad()
        {
            InitializeComponent();
        }

        dbRegistrationOfParticipationInOlympiadsEntities db = new dbRegistrationOfParticipationInOlympiadsEntities();

        private void FormNewOlympiad_Load(object sender, EventArgs e)
        {
            cbSubject.DataSource = db.SubjectTables.OrderBy(n => n.Subject).Select(n => n.Subject).ToList();
            cbSubject.SelectedIndex = -1;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            List<string> nameSQL = new List<string>();
            List<string> nameSQLNew = new List<string>();
            nameSQL = db.OlympiadTables.Select(n => n.OlympiadName).ToList();
            string nameTextBox = "";
            foreach(char c in tbNameOlympiad.Text)
            {
                if(char.IsLetter(c))
                {
                    nameTextBox += c;
                }
            }
            for (int i = 0; i< nameSQL.Count; i++)
            {
                nameSQLNew.Add("");
                foreach(char c in nameSQL[i])
                if (char.IsLetter(c))
                {
                        nameSQLNew[i] += c;
                }
                nameSQLNew[i] = nameSQLNew[i].ToLower();
            }

            if (!String.IsNullOrEmpty(tbDescription.Text) && !String.IsNullOrEmpty(tbNameOlympiad.Text) && !String.IsNullOrEmpty(cbSubject.Text))
            {
                if (!nameSQLNew.Contains(nameTextBox.ToLower()))
                {
                    if (db.SubjectTables.Where(n => n.Subject == cbSubject.Text).Count() == 0)
                    {
                        SubjectTable subjectTable = new SubjectTable
                        {
                            Subject = cbSubject.Text
                        };
                        db.SubjectTables.Add(subjectTable);
                        db.SaveChanges();
                    }
                    OlympiadTable olympiadTable = new OlympiadTable
                    {
                        OlympiadName = tbNameOlympiad.Text,
                        SubjectId = db.SubjectTables.FirstOrDefault(n => n.Subject == cbSubject.Text).SubjectId,
                        Description = tbDescription.Text,
                        Team = cbTeam.Checked
                    };
                    db.OlympiadTables.Add(olympiadTable);
                    db.SaveChanges();
                    MessageBox.Show("Олимпиада добавлена.", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    exit = true;
                    DialogResult = DialogResult.OK;
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Олимпиада с таким названием уже существует", "Ошибка", MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("Заполните все поля", "Ошибка", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }

        public bool exit = false;

        private void FormNewOlympiad_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!exit)
            {
                if ((MessageBox.Show("Вы уверены, что хотите отменить действие?", "Внимание!",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question)) == DialogResult.Yes)
                {
                    exit = true;
                }
                else
                {
                    e.Cancel = true;
                }
            }
        }

        private void cbSubject_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsLetter(e.KeyChar) && e.KeyChar != 8)
            {
                e.Handled = true;
            }
        }
    }
}
