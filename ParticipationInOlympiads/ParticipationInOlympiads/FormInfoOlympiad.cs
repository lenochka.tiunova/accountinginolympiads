﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParticipationInOlympiads
{
    public partial class FormInfoOlympiad : Form
    {
        public FormInfoOlympiad()
        {
            InitializeComponent();
        }

        dbRegistrationOfParticipationInOlympiadsEntities db = new dbRegistrationOfParticipationInOlympiadsEntities();
        public int index = -1;

        private void FormInfoOlympiad_Load(object sender, EventArgs e)
        {
            tbNameOlympiad.Text = db.OlympiadPlanTables.FirstOrDefault(n => n.OlympiadPlanId == index)
                    .OlimpiadByAgeAndLevelTable.OlympiadByAgeTable.OlympiadTable.OlympiadName;
            tbSubject.Text = db.OlympiadPlanTables.FirstOrDefault(n => n.OlympiadPlanId == index)
                    .OlimpiadByAgeAndLevelTable.OlympiadByAgeTable.OlympiadTable.SubjectTable.Subject;
            tblDescription.Text = db.OlympiadPlanTables.FirstOrDefault(n => n.OlympiadPlanId == index)
                    .OlimpiadByAgeAndLevelTable.OlympiadByAgeTable.OlympiadTable.Description;
            if(db.OlympiadPlanTables.FirstOrDefault(n => n.OlympiadPlanId == index)
                    .OlimpiadByAgeAndLevelTable.OlympiadByAgeTable.OlympiadTable.Team)
            {
                lblTeam.Text = "Олимпиада командная: Да";
            }
            else
            {
                lblTeam.Text = "Олимпиада командная: Нет";
            }
        }
    }
}
