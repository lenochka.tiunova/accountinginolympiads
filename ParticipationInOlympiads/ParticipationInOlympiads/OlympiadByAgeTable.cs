//------------------------------------------------------------------------------
// <auto-generated>
//     Этот код создан по шаблону.
//
//     Изменения, вносимые в этот файл вручную, могут привести к непредвиденной работе приложения.
//     Изменения, вносимые в этот файл вручную, будут перезаписаны при повторном создании кода.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ParticipationInOlympiads
{
    using System;
    using System.Collections.Generic;
    
    public partial class OlympiadByAgeTable
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public OlympiadByAgeTable()
        {
            this.OlimpiadByAgeAndLevelTables = new HashSet<OlimpiadByAgeAndLevelTable>();
        }
    
        public int OlympiadByAgeId { get; set; }
        public int OlympiadId { get; set; }
        public int SchoolStageId { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OlimpiadByAgeAndLevelTable> OlimpiadByAgeAndLevelTables { get; set; }
        public virtual OlympiadTable OlympiadTable { get; set; }
        public virtual SchoolStageTable SchoolStageTable { get; set; }
    }
}
