﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Word = Microsoft.Office.Interop.Word;
using Excel = Microsoft.Office.Interop.Excel;
using Microsoft.Office.Interop.Excel;
using System.Globalization;
using Microsoft.Office.Interop.Word;
using System.Text.RegularExpressions;

namespace ParticipationInOlympiads
{
    public partial class FormAccountingForParticipation : Form
    {
        static dbRegistrationOfParticipationInOlympiadsEntities db = new dbRegistrationOfParticipationInOlympiadsEntities();
        public FormAccountingForParticipation()
        {
            InitializeComponent();
            miPlan_Click(null, null);
        }

        public void Add(CheckedListBox clbStage, CheckedListBox clbLevel, ComboBox cbSubject)
        {
            var stage = db.SchoolStageTables.Select(n => n.SchoolStage).ToList();
            clbStage.Items.Clear();
            clbStage.Items.Add("Все");
            foreach (string v in stage)
            {
                clbStage.Items.Add(v);
            }
            var level = db.LevelTables.Select(n => n.OlympiadLevel).ToList();
            clbLevel.Items.Clear();
            clbLevel.Items.Add("Все");
            foreach (string v in level)
            {
                clbLevel.Items.Add(v);
            }

            var subject = db.SubjectTables.OrderBy(n => n.Subject).Select(n => n.Subject).ToList();
            cbSubject.Items.Clear();
            cbSubject.Items.Add("Выбрать все");
            foreach (string sub in subject)
            {
                cbSubject.Items.Add(sub);
            }
            cbSubject.SelectedIndex = 0;
        }

        List<string> name;

        public void AddAccounting()
        {
            var students = db.StudentTables.OrderBy(n => n.StudentSurname)
                .Select(n => new { n.StudentSurname, n.StudentName, n.StudentMiddle }).ToList();
            name = new List<string>();
            cbNameStudent.Items.Clear();
            for (int i = 0; i < students.Count; i++)
            {
                name.Add(students[i].StudentSurname + " " + students[i].StudentName
                    + " " + students[i].StudentMiddle);
                cbNameStudent.Items.Add(students[i].StudentSurname + " " + students[i].StudentName
                    + " " + students[i].StudentMiddle);
            }
            cbNameStudent.SelectedIndex = -1;

            var classStudent = db.ClassTables.Select(n => n.Class).ToList();
            clbClass.Items.Clear();
            clbClass.Items.Add("Все");
            foreach (byte v in classStudent)
            {
                clbClass.Items.Add(v);
            }
        }

        public void viewAccountingEdit(bool check)
        {
            miResult.Visible = check;
            pnlAccounting.Visible = check;
            miAddStudent.Visible = check;
            miCancelParticipation.Visible = check;
        }

        public void viewEdit(bool check)
        {
            pnlPlanOlympiad.Visible = check;
            miEditData.Visible = check;
            miAddOlympiad.Visible = check;
            miViewInfo.Visible = check;
            miCancel.Visible = check;
            miEditData.Visible = check;
        }

        bool questions = true;

        private void miAccounting_Click(object sender, EventArgs e)
        {
            this.Text = "Учет участия в олимпиадах. Учет участников олимпиад";
            table = "accounting";
            Add(clbStageOlympiad, clblLevelOlympiad, cbSubjectForRegisration);
            AddAccounting();
            btnClearAll_Click(null, null);
            viewAccountingEdit(true);
            viewEdit(false);
            var accounting = db.RegistrationOfParticipantsTables
                .Select(n => new
                {
                    n.RegistrationOfParticipantsId,
                    n.OlympiadPlanId,
                    n.OlympiadPlanTable.OlimpiadByAgeAndLevelTable.OlympiadByAgeTable.OlympiadTable.SubjectTable.Subject,
                    n.OlympiadPlanTable.OlimpiadByAgeAndLevelTable.OlympiadByAgeTable.OlympiadTable.OlympiadName,
                    n.OlympiadPlanTable.OlimpiadByAgeAndLevelTable.OlympiadByAgeTable.SchoolStageTable.SchoolStage,
                    n.OlympiadPlanTable.OlimpiadByAgeAndLevelTable.LevelTable.OlympiadLevel,
                    team = n.OlympiadPlanTable.OlimpiadByAgeAndLevelTable.OlympiadByAgeTable.OlympiadTable.Team,
                    date = n.OlympiadPlanTable.Date,
                    n.StudentTable.StudentSurname,
                    n.StudentTable.StudentName,
                    n.StudentTable.StudentMiddle,
                    n.Class,
                    n.Scores,
                    n.ResultTable.Result,
                    n.RewardTable.Reward,
                    n.Absent
                })
                .OrderByDescending(n => new { n.date, n.OlympiadName, n.Scores })
                .ToList();
            dgvAccounting.DataSource = accounting;
            InterfaceDataGridViewForAccounting();
            if (questions)
            {
                if (db.RegistrationOfParticipantsTables
                    .FirstOrDefault(n => n.Absent == null && n.OlympiadPlanTable.Date < DateTime.Now) != null)
                {
                    if (MessageBox.Show("Есть записи с незафиксированными результатами\r\nОткрыть окно для фиксации результата?",
                        "Внимание", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        FormNewParticipant newParticipant = new FormNewParticipant();
                        newParticipant.index = db.RegistrationOfParticipantsTables
                            .FirstOrDefault(n => n.Absent == null && n.OlympiadPlanTable.Date < DateTime.Now).RegistrationOfParticipantsId;
                        newParticipant.Text = "Фиксация результата";
                        newParticipant.ShowDialog();
                        questions = false;
                        miAccounting_Click(null, null);
                    }
                }
            }
            else
            {
                questions = true;
            }
        }

        public void InterfaceDataGridViewForPlan()
        {
            dgvAccounting.Columns[0].Visible = false;
            dgvAccounting.Columns[1].HeaderText = "Название";
            dgvAccounting.Columns[2].HeaderText = "Предмет";
            dgvAccounting.Columns[3].HeaderText = "Область классов";
            dgvAccounting.Columns[4].HeaderText = "Уровень";
            dgvAccounting.Columns[5].HeaderText = "Дата";
        }

        public void InterfaceDataGridViewForAccounting()
        {
            dgvAccounting.Columns[0].Visible = false;
            dgvAccounting.Columns[1].Visible = false;
            dgvAccounting.Columns[2].Visible = false;
            dgvAccounting.Columns[3].HeaderText = "Олимпиада";
            dgvAccounting.Columns[4].HeaderText = "Область классов";
            dgvAccounting.Columns[5].HeaderText = "Уровень";
            dgvAccounting.Columns[6].HeaderText = "Командная";
            dgvAccounting.Columns[7].HeaderText = "Дата";
            dgvAccounting.Columns[8].HeaderText = "Фамилия";
            dgvAccounting.Columns[9].HeaderText = "Имя";
            dgvAccounting.Columns[10].HeaderText = "Отчество";
            dgvAccounting.Columns[11].HeaderText = "Класс";
            dgvAccounting.Columns[12].HeaderText = "Баллы";
            dgvAccounting.Columns[12].DefaultCellStyle.Format = "N2";
            dgvAccounting.Columns[13].HeaderText = "Результат";
            dgvAccounting.Columns[14].HeaderText = "Вознаграждение";
            dgvAccounting.Columns[15].HeaderText = "Ученик отсутствовал";
        }

        string table;

        private void miPlan_Click(object sender, EventArgs e)
        {
            this.Text = "Учет участия в олимпиадах. План проведения олимпиад";
            Add(clbStage, clbLevel, cbSubject);
            btnDroppAll_Click(null, null);
            viewAccountingEdit(false);
            viewEdit(true);
            var plan = db.OlympiadPlanTables
                .Select(n => new
                {
                    n.OlympiadPlanId,
                    n.OlimpiadByAgeAndLevelTable.OlympiadByAgeTable.OlympiadTable.OlympiadName,
                    n.OlimpiadByAgeAndLevelTable.OlympiadByAgeTable.OlympiadTable.SubjectTable.Subject,
                    n.OlimpiadByAgeAndLevelTable.OlympiadByAgeTable.SchoolStageTable.SchoolStage,
                    n.OlimpiadByAgeAndLevelTable.LevelTable.OlympiadLevel,
                    n.Date
                })
                .OrderBy(n => new { n.Date, n.OlympiadName, n.SchoolStage})
                .ToList();
            dgvAccounting.DataSource = plan;
            table = "plan";
            InterfaceDataGridViewForPlan();
        }

        public void Filter()
        {
            var search = db.OlympiadPlanTables
                .Select(n => new
                {
                    n.OlympiadPlanId,
                    n.OlimpiadByAgeAndLevelTable.OlympiadByAgeTable.OlympiadTable.OlympiadName,
                    n.OlimpiadByAgeAndLevelTable.OlympiadByAgeTable.OlympiadTable.SubjectTable.Subject,
                    n.OlimpiadByAgeAndLevelTable.OlympiadByAgeTable.SchoolStageTable.SchoolStage,
                    n.OlimpiadByAgeAndLevelTable.LevelTable.OlympiadLevel,
                    n.Date
                });

            search = search
           .Where(n => n.OlympiadName.Contains(tbFind.Text));

            List<string> list = new List<string>();

            if (clbStage.CheckedItems.Count != 0)
            {
                for (int i = 0; i < clbStage.Items.Count; i++)
                {
                    if (clbStage.GetItemCheckState(i) == CheckState.Checked)
                    {
                        list.Add(clbStage.Items[i].ToString());
                    }
                }

                search = search
               .Where(n => list.Any(m => m.ToString() == n.SchoolStage));
            }

            if (clbLevel.CheckedItems.Count != 0)
            {

                for (int i = 0; i < clbLevel.Items.Count; i++)
                {
                    if (clbLevel.GetItemCheckState(i) == CheckState.Checked)
                    {
                        list.Add(clbLevel.Items[i].ToString());
                    }
                }

                search = search
                       .Where(n => list.Any(m => m.ToString() == n.OlympiadLevel));
            }

            if (cbSubject.SelectedIndex != 0 && cbSubject.SelectedIndex != -1)
            {
                search = search
                       .Where(n => n.Subject.Equals(cbSubject.SelectedItem.ToString()));
            }

            search = search.Where(n => n.Date >= dtpStartDate.Value && n.Date <= dtpEndDate.Value);

            dgvAccounting.DataSource = search.OrderBy(n => new { n.Date, n.OlympiadName, n.SchoolStage }).ToList();
            InterfaceDataGridViewForPlan();
        }

        private void tbFind_TextChanged(object sender, EventArgs e)
        {
            Filter();
        }

        private void clbStage_SelectedIndexChanged(object sender, EventArgs e)
        {
            CheckListAll(clbStage);
            Filter();
        }

        private void clbLevel_SelectedIndexChanged(object sender, EventArgs e)
        {
            CheckListAll(clbLevel);
            Filter();
        }

        private void cbSubject_SelectedIndexChanged(object sender, EventArgs e)
        {
            Filter();
        }

        private void rbClass_CheckedChanged(object sender, EventArgs e)
        {
            if (rbClass.Checked)
            {
                for (int i = 0; i < clbStageOlympiad.Items.Count; i++)
                {
                    clbStageOlympiad.SetItemChecked(i, false);
                }
                clbStageOlympiad.ClearSelected();
                clbStageOlympiad.Enabled = false;
                clbClass.Enabled = true;
                FilterForAccounting();
            }
        }

        private void rbStage_CheckedChanged(object sender, EventArgs e)
        {
            if (rbStage.Checked)
            {
                for (int i = 0; i < clbClass.Items.Count; i++)
                {
                    clbClass.SetItemChecked(i, false);
                }
                clbClass.ClearSelected();
                clbClass.Enabled = false;
                clbStageOlympiad.Enabled = true;
                FilterForAccounting();
            }
        }

        public void FilterForAccounting()
        {
            var search = db.RegistrationOfParticipantsTables
                .Select(n => new
                {
                    n.RegistrationOfParticipantsId,
                    n.OlympiadPlanId,
                    n.OlympiadPlanTable.OlimpiadByAgeAndLevelTable.OlympiadByAgeTable.OlympiadTable.SubjectTable.Subject,
                    n.OlympiadPlanTable.OlimpiadByAgeAndLevelTable.OlympiadByAgeTable.OlympiadTable.OlympiadName,
                    n.OlympiadPlanTable.OlimpiadByAgeAndLevelTable.OlympiadByAgeTable.SchoolStageTable.SchoolStage,
                    n.OlympiadPlanTable.OlimpiadByAgeAndLevelTable.LevelTable.OlympiadLevel,
                    n.OlympiadPlanTable.OlimpiadByAgeAndLevelTable.OlympiadByAgeTable.OlympiadTable.Team,
                    date = n.OlympiadPlanTable.Date,
                    n.StudentTable.StudentSurname,
                    n.StudentTable.StudentName,
                    n.StudentTable.StudentMiddle,
                    n.Class,
                    n.Scores,
                    n.ResultTable.Result,
                    n.RewardTable.Reward,
                    n.Absent
                });

            search = search
           .Where(n => n.OlympiadName.Contains(tbName.Text));

            List<string> list = new List<string>();

            if (clbStageOlympiad.CheckedItems.Count != 0)
            {
                for (int i = 1; i < clbStageOlympiad.Items.Count; i++)
                {
                    if (clbStageOlympiad.GetItemCheckState(i) == CheckState.Checked)
                    {
                        list.Add(clbStageOlympiad.Items[i].ToString());
                    }
                }

                search = search
               .Where(n => list.Any(m => m.ToString() == n.SchoolStage));
            }

            if (clblLevelOlympiad.CheckedItems.Count != 0)
            {

                for (int i = 1; i < clblLevelOlympiad.Items.Count; i++)
                {
                    if (clblLevelOlympiad.GetItemCheckState(i) == CheckState.Checked)
                    {
                        list.Add(clblLevelOlympiad.Items[i].ToString());
                    }
                }

                search = search
                       .Where(n => list.Any(m => m.ToString() == n.OlympiadLevel));
            }

            if (clbClass.CheckedItems.Count != 0)
            {

                for (int i = 1; i < clbClass.Items.Count; i++)
                {
                    if (clbClass.GetItemCheckState(i) == CheckState.Checked)
                    {
                        list.Add(clbClass.Items[i].ToString());
                    }
                }

                search = search
                       .Where(n => list.Any(m => m.ToString() == n.Class.ToString()));
            }

            if (cbNameStudent.SelectedIndex != -1)
            {
                string name = cbNameStudent.SelectedItem.ToString().Split(' ')[1];
                string surname = cbNameStudent.SelectedItem.ToString().Split(' ')[0];
                string middle = cbNameStudent.SelectedItem.ToString().Split(' ')[2];

                search = search
                       .Where(n => n.StudentName.Equals(name))
                       .Where(n => n.StudentSurname.Equals(surname))
                       .Where(n => n.StudentMiddle.Equals(middle));
            }

            if (cbSubjectForRegisration.SelectedIndex != 0)
            {
                search = search
                       .Where(n => n.Subject.Equals(cbSubjectForRegisration.SelectedItem.ToString()));
            }

            search = search.Where(n => n.date >= dtpStart.Value && n.date <= dtpEnd.Value);

            dgvAccounting.DataSource = search
                .OrderByDescending(n => new { n.date, n.OlympiadName, n.Scores }).ToList();
            InterfaceDataGridViewForAccounting();
        }

        private void CheckListAll(CheckedListBox listBox)
        {
            if (listBox.SelectedIndex == 0)
            {
                bool all = false;
                if (listBox.GetItemCheckState(0) == CheckState.Checked)
                {
                    all = true;
                }
                for (int i = 1; i < listBox.Items.Count; i++)
                {
                    listBox.SetItemChecked(i, all);
                }
            }
        }

        private void tbName_TextChanged(object sender, EventArgs e)
        {
            FilterForAccounting();
        }

        private void clblLevelOlympiad_SelectedIndexChanged(object sender, EventArgs e)
        {
            CheckListAll(clblLevelOlympiad);
            FilterForAccounting();
        }

        private void clbClass_SelectedIndexChanged(object sender, EventArgs e)
        {
            CheckListAll(clbClass);
            FilterForAccounting();
        }

        private void clbStageOlympiad_SelectedIndexChanged(object sender, EventArgs e)
        {
            CheckListAll(clbStageOlympiad);
            FilterForAccounting();
        }

        Regex regex;

        private void cbNameStudent_SelectedIndexChanged(object sender, EventArgs e)
        {
            FilterForAccounting();
        }

        private void dtpStart_ValueChanged(object sender, EventArgs e)
        {
            FilterForAccounting();
        }

        private void dtpEnd_ValueChanged(object sender, EventArgs e)
        {
            FilterForAccounting();
        }

        public void DateSetting(DateTimePicker start, DateTimePicker end)
        {
            int year;
            if (DateTime.Now.Month > 8)
            {
                year = DateTime.Now.Year;
            }
            else
            {
                year = DateTime.Now.Year - 1;
            }
            start.Value = new DateTime(year, 09, 01);
            end.Value = new DateTime(year + 1, 05, 31);
        }

        private void btnDropping_Click(object sender, EventArgs e)
        {
            DateSetting(dtpStart, dtpEnd);
        }

        private void dgvAccounting_DataSourceChanged(object sender, EventArgs e)
        {
            lblCount.Text = "Количество записей: " + dgvAccounting.Rows.Count;
        }

        private void miAddOlympiad_Click(object sender, EventArgs e)
        {
            FormChangesPlan changesPlan = new FormChangesPlan();
            changesPlan.Text = "Регистрация олимпиады";
            changesPlan.ShowDialog();
            if (changesPlan.DialogResult == DialogResult.Yes)
            {
                miPlan_Click(null, null);
            }
        }

        private void miEditDate_Click(object sender, EventArgs e)
        {
            var index = int.Parse(dgvAccounting[0, dgvAccounting.CurrentRow.Index].Value.ToString());
            if (db.OlympiadPlanTables.FirstOrDefault(n => n.OlympiadPlanId == index && n.Date > DateTime.Today) != null)
            {
                FormChangesPlan changesPlan = new FormChangesPlan();
                changesPlan.Text = "Изменение данных об олимпиаде";
                changesPlan.index = int.Parse(dgvAccounting.Rows[dgvAccounting.CurrentCell.RowIndex].Cells[0].Value.ToString());
                changesPlan.ShowDialog();
                if (changesPlan.DialogResult == DialogResult.Yes)
                {
                    miPlan_Click(null, null);
                }
            }
            else
            {
                MessageBox.Show("Данная олимпиада уже прошла", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void cbSubjectForRegisration_SelectedIndexChanged(object sender, EventArgs e)
        {
            FilterForAccounting();
        }

        private void miExcel_Click(object sender, EventArgs e)
        {
            saveExcel.FileName = "Книга";
            if (dgvAccounting.Rows.Count > 0)
            {
                saveExcel.ShowDialog();
            }
            else
            {
                MessageBox.Show("Нет данных для экспорта", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void saveExcel_FileOk(object sender, CancelEventArgs e)
        {
            try
            {
                Excel.Application exApp = new Excel.Application();
                exApp.Visible = true;
                exApp.Workbooks.Add();
                Worksheet worksheet = (Worksheet)exApp.ActiveSheet;
                string name = Text.Substring(this.Text.IndexOf(". ") + 2);
                worksheet.Name = name;

                int startColumn;
                if (table == "plan")
                {
                    startColumn = 1;
                }
                else
                {
                    startColumn = 3;
                }

                int k = 1;
                for (int j = startColumn; j < dgvAccounting.ColumnCount; j++)
                {
                    worksheet.Cells[2, k] = dgvAccounting.Columns[j].HeaderCell.Value.ToString();
                    k++;
                }

                for (int i = 1; i <= dgvAccounting.RowCount; i++)
                {
                    k = 1;
                    for (int j = startColumn; j < dgvAccounting.ColumnCount; j++)
                    {
                        if (dgvAccounting[j, i - 1].Value != null)
                        {
                            worksheet.Cells[i + 2, k] = dgvAccounting[j, i - 1].Value.
                                ToString().Trim()
                            .Replace("\r\n", "")
                            .Replace(" 0:00:00", "").Replace("False", "Нет").Replace("True", "Да");
                        }
                        else
                        {
                            worksheet.Cells[i + 2, j] = "";
                        }
                        k++;
                    }
                }
                worksheet.Columns.AutoFit();

                Compound(worksheet, 1, ((char)(dgvAccounting.ColumnCount - (startColumn + 1) + 'A')).ToString());
                worksheet.Cells[1, 1] = name;
                worksheet.Cells[1, 1].HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;

                Compound(worksheet, dgvAccounting.RowCount + 4, ((char)(dgvAccounting.ColumnCount - (startColumn + 1) + 'A')).ToString());
                Compound(worksheet, dgvAccounting.RowCount + 5, ((char)(dgvAccounting.ColumnCount - (startColumn + 1) + 'A')).ToString());

                worksheet.Cells[dgvAccounting.RowCount + 4, 1] = "БОУ ОО СОШ №15";
                worksheet.Cells[dgvAccounting.RowCount + 5, 1] = "Дата формирования отчета: " + DateTime.Now.ToShortDateString();

                worksheet.SaveAs(saveExcel.FileName);
            }
            catch
            {

            }
        }

        public void Compound(Worksheet worksheet, int row, string column)
        {
            Excel.Range _excelCells1 = worksheet.get_Range("A" + row, column + row).Cells;
            _excelCells1.Merge(Type.Missing);
        }

        private void miAddStudent_Click(object sender, EventArgs e)
        {
            FormNewParticipant newParticipant = new FormNewParticipant();
            newParticipant.pnlRegistration.Visible = false;
            newParticipant.ShowDialog();
            questions = false;
            miAccounting_Click(null, null);

            if(newParticipant.DialogResult == DialogResult.Yes)
            {
                miAddStudent_Click(null,null);
            }
        }

        private void miViewInfo_Click(object sender, EventArgs e)
        {
            FormInfoOlympiad infoOlympiad = new FormInfoOlympiad();
            infoOlympiad.index = int.Parse(dgvAccounting.Rows[dgvAccounting.CurrentCell.RowIndex].Cells[0].Value.ToString());
            infoOlympiad.Show();
        }

        public void ClearCheckListBox(CheckedListBox checkedListBox)
        {
            for (int i = 0; i < checkedListBox.Items.Count; i++)
            {
                checkedListBox.SetItemChecked(i, false);
            }
            checkedListBox.SelectedIndex = -1;
        }

        private void btnDroppAll_Click(object sender, EventArgs e)
        {
            tbFind.Clear();
            ClearCheckListBox(clbLevel);
            ClearCheckListBox(clbStage);
            cbSubject.SelectedIndex = 0;
            DateSetting(dtpStartDate, dtpEndDate);
        }

        private void btnClearAll_Click(object sender, EventArgs e)
        {
            tbName.Text = " ";
            tbName.Clear();
            ClearCheckListBox(clbStageOlympiad);
            ClearCheckListBox(clbClass);
            ClearCheckListBox(clblLevelOlympiad);
            DateSetting(dtpStart, dtpEnd);
            cbNameStudent.SelectedIndex = -1;
            cbSubjectForRegisration.SelectedIndex = 0;

        }

        bool export = false;

        private void miWord_Click(object sender, EventArgs e)
        {
            saveWord.FileName = "Документ Microsoft Word";
            if (dgvAccounting.Rows.Count > 0)
            {
                export = true;
                saveWord.ShowDialog();
            }
            else
            {
                MessageBox.Show("Нет данных для экспорта", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void saveWord_FileOk(object sender, CancelEventArgs e)
        {
            try
            {
                if (export)
                {
                    Word.Application objWord = new Word.Application();
                    objWord.Visible = true;
                    Word.Document Odoc;
                    object objMissing = System.Reflection.Missing.Value;
                    Odoc = objWord.Documents.Add(ref objMissing, ref objMissing, ref objMissing);

                    Object begin = 0;
                    Object end = 0;
                    Word.Range range = Odoc.Range(ref begin, ref end);
                    if (table != "plan")
                    {
                        range.PageSetup.Orientation = Word.WdOrientation.wdOrientLandscape;
                    }
                    range.Text = "Дата формирования отчета: " + DateTime.Now.ToShortDateString();
                    range.Font.Size = 12;
                    range.ParagraphFormat.Alignment = Word.WdParagraphAlignment.wdAlignParagraphRight;

                    Odoc.Paragraphs.Add(ref objMissing);
                    Odoc.Paragraphs[2].Range.Text = "БОУ ОО СОШ №15\r\n";
                    Odoc.Paragraphs[2].Range.ParagraphFormat.Alignment = Word.WdParagraphAlignment.wdAlignParagraphCenter;
                    Odoc.Paragraphs[2].Range.Bold = 1;
                    Odoc.Paragraphs[2].Range.Font.Size = 12;

                    Odoc.Paragraphs.Add(ref objMissing);
                    Odoc.Paragraphs[3].Range.Text = Text.Substring(this.Text.IndexOf(". ") + 2);
                    Odoc.Paragraphs[3].Range.ParagraphFormat.Alignment = Word.WdParagraphAlignment.wdAlignParagraphCenter;
                    Odoc.Paragraphs[3].Range.Bold = 1;
                    Odoc.Paragraphs[3].Range.Font.Size = 12;

                    int startColumn;
                    if (table == "plan")
                    {
                        startColumn = 1;
                    }
                    else
                    {
                        startColumn = 3;
                    }

                    Word.Paragraph wordPar;
                    wordPar = Odoc.Paragraphs.Add(ref objMissing);
                    Word.Range wordrange = Odoc.Paragraphs[4].Range;
                    wordrange.Font.Size = 12;
                    wordrange.Bold = 0;
                    Word.Table wordtable1 = Odoc.Tables.Add(wordrange, dgvAccounting.RowCount + 1, dgvAccounting.ColumnCount - startColumn,
                    Word.WdDefaultTableBehavior.wdWord9TableBehavior, Word.WdAutoFitBehavior.wdAutoFitWindow);

                    wordtable1.Borders.OutsideLineStyle = Word.WdLineStyle.wdLineStyleSingle;
                    wordtable1.Borders.InsideLineStyle = Word.WdLineStyle.wdLineStyleSingle;
                    wordtable1.Range.ParagraphFormat.Alignment = Word.WdParagraphAlignment.wdAlignParagraphCenter;

                    int k = 1;
                    for (int j = startColumn; j < dgvAccounting.ColumnCount; j++)
                    {
                        wordtable1.Cell(1, k).Range.Text = dgvAccounting.Columns[j].HeaderCell.Value.ToString();
                        wordtable1.Cell(1, k).Range.Bold = 5;
                        k++;
                    }

                    for (int i = 1; i <= dgvAccounting.RowCount; i++)
                    {
                        k = 1;
                        for (int j = startColumn; j < dgvAccounting.ColumnCount; j++)
                        {
                            if (dgvAccounting[j, i - 1].Value != null)
                            {
                                wordtable1.Cell(i + 1, k).Range.Text = dgvAccounting[j, i - 1].Value.ToString()
                                    .Replace(" 0:00:00", "").Replace("False", "Нет").Replace("True", "Да");
                            }
                            else
                            {
                                wordtable1.Cell(i + 1, k).Range.Text = "";
                            }
                            k++;
                        }
                    }

                    Odoc.SaveAs(saveWord.FileName);
                }
                else
                {
                    Word.Application objWord = new Word.Application();
                    objWord.Visible = true;
                    Word.Document Odoc;
                    object objMissing = System.Reflection.Missing.Value;
                    Odoc = objWord.Documents.Add(ref objMissing, ref objMissing, ref objMissing);

                    Object begin = 0;
                    Object end = 0;
                    Word.Range range = Odoc.Range(ref begin, ref end);
                    range.Text = "Заявка на участие в олимпиаде школьников\r\n\"" + db
                        .OlympiadPlanTables.FirstOrDefault(n => n.OlympiadPlanId == olympiadIndex).OlimpiadByAgeAndLevelTable
                        .OlympiadByAgeTable.OlympiadTable.OlympiadName + "\"\r\nот БОУ ОО СОШ №15";
                    range.Font.Size = 14;
                    range.Bold = 1;
                    range.ParagraphFormat.Alignment = Word.WdParagraphAlignment.wdAlignParagraphCenter;

                    Word.Paragraph wordPar;
                    wordPar = Odoc.Paragraphs.Add(ref objMissing);
                    Word.Range wordrange = Odoc.Paragraphs[4].Range;
                    wordrange.Font.Size = 12;
                    wordrange.Bold = 0;

                    var participants = db.RegistrationOfParticipantsTables.Where(n => n.OlympiadPlanId == olympiadIndex).ToList();

                    Word.Table wordtable1 = wordrange.Tables.Add(wordrange, participants.Count + 1, 3,
                    Word.WdDefaultTableBehavior.wdWord9TableBehavior, Word.WdAutoFitBehavior.wdAutoFitWindow);

                    wordtable1.Borders.OutsideLineStyle = Word.WdLineStyle.wdLineStyleSingle;
                    wordtable1.Borders.InsideLineStyle = Word.WdLineStyle.wdLineStyleSingle;
                    wordtable1.Range.ParagraphFormat.Alignment = Word.WdParagraphAlignment.wdAlignParagraphCenter;

                    wordtable1.Cell(1, 1).Range.Text = "№ п/п";
                    wordtable1.Cell(1, 2).Range.Text = "ФИО ученика";
                    wordtable1.Cell(1, 3).Range.Text = "Класс";

                    for (int i = 1; i <= participants.Count; i++)
                    {
                        wordtable1.Cell(i + 1, 1).Range.Text = i.ToString();
                        wordtable1.Cell(i + 1, 2).Range.Text = participants[i -1 ].StudentTable.StudentSurname + " "
                            + participants[i - 1].StudentTable.StudentName + " " + participants[i - 1].StudentTable.StudentMiddle;
                        wordtable1.Cell(i + 1, 3).Range.Text = participants[i - 1].StudentTable.ClassTable.Class.ToString();
                    }

                    Word.Paragraph wordPar2;
                   
                    wordPar2 = Odoc.Paragraphs.Add();
                    wordPar2.Range.Text = "\r\nДиректор школы\t\t\t\t\t\t\t\t\t" + "Коровин С.В.";
                    wordPar2.Range.Font.Size = 12;
                    wordPar2.Range.ParagraphFormat.Alignment = Word.WdParagraphAlignment.wdAlignParagraphJustify;

                    Odoc.SaveAs(saveWord.FileName);
                }
            }
            catch
            {

            }
        }

        private void miCancel_Click(object sender, EventArgs e)
        {
            var index = int.Parse(dgvAccounting[0, dgvAccounting.CurrentRow.Index].Value.ToString());
            if (db.OlympiadPlanTables.FirstOrDefault(n => n.OlympiadPlanId == index && n.Date > DateTime.Now) != null)
            {
                if (MessageBox.Show("Вы уверены, что хотите Удалить запись?", "Внимание!", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    var plan = db.OlympiadPlanTables.FirstOrDefault(n => n.OlympiadPlanId == index);
                    db.OlympiadPlanTables.Remove(plan);
                    db.SaveChanges();
                    miPlan_Click(null, null);
                }
            }
            else
            {
                MessageBox.Show("Данная олимпиада уже прошла", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void miResult_Click(object sender, EventArgs e)
        {
            if (dgvAccounting.CurrentRow != null)
            {
                var index = int.Parse(dgvAccounting[0, dgvAccounting.CurrentRow.Index].Value.ToString());
                if (db.RegistrationOfParticipantsTables
                        .FirstOrDefault(n => n.RegistrationOfParticipantsId == index && n.OlympiadPlanTable.Date > DateTime.Today
                        || (n.RegistrationOfParticipantsId == index && (n.Scores != null || n.Absent != null))) == null)
                {
                    FormNewParticipant newParticipant = new FormNewParticipant();
                    newParticipant.index = int.Parse(dgvAccounting.Rows[dgvAccounting.CurrentRow.Index].Cells[0].Value.ToString());
                    newParticipant.Text = "Фиксация результата";
                    newParticipant.ShowDialog();
                    questions = false;
                    miAccounting_Click(null, null);
                }

                else
                {
                    MessageBox.Show("Результат для данной записи зафиксировать нельзя", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                MessageBox.Show("Сначала выберите запись для фиксации результата", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void dgvAccounting_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (table == "plan")
            {
                for (int i = 0; i < dgvAccounting.Rows.Count; i++)
                {
                    if (Convert.ToDateTime(dgvAccounting.Rows[i].Cells[5].Value) >= DateTime.Today)
                    {
                        for (int j = i; j < dgvAccounting.Rows.Count; j++)
                        {
                            if (Convert.ToDateTime(dgvAccounting.Rows[j].Cells[5].Value) <
                                Convert.ToDateTime(dgvAccounting.Rows[i].Cells[5].Value).AddMonths(1))
                            {
                                dgvAccounting.Rows[j].DefaultCellStyle.BackColor = Color.Thistle;
                            }
                            else
                            {
                                break;
                            }
                        }
                        break;
                    }
                }
            }
            else
            {
                if (e.ColumnIndex == 15)
                {
                    if (e.Value is bool)
                    {
                        bool value = (bool)e.Value;
                        e.Value = (value) ? "Да" : "Нет";
                        e.FormattingApplied = true;
                    }
                }

                if (dgvAccounting.Columns.Count > 14)
                {
                    for (int i = 0; i < dgvAccounting.Rows.Count; i++)
                    {
                        if (Convert.ToDateTime(dgvAccounting.Rows[i].Cells[7].Value) > DateTime.Now)
                        {
                            dgvAccounting.Rows[i].Cells[3].Style.BackColor = Color.Thistle;
                        }
                        else
                        {
                            dgvAccounting.Rows[i].Cells[3].Style.BackColor = Color.White;
                        }
                    }
                }
            }
        }

        private void dgvAccounting_DoubleClick(object sender, EventArgs e)
        {
            if (this.Text == "Учет участия в олимпиадах. План проведения олимпиад")
            {
                int index = int.Parse(dgvAccounting.Rows[dgvAccounting.CurrentRow.Index].Cells[0].Value.ToString());

                var search = db.RegistrationOfParticipantsTables
                    .Select(n => new
                    {
                        n.RegistrationOfParticipantsId,
                        n.OlympiadPlanId,
                        n.OlympiadPlanTable.OlimpiadByAgeAndLevelTable.OlympiadByAgeTable.OlympiadTable.SubjectTable.Subject,
                        n.OlympiadPlanTable.OlimpiadByAgeAndLevelTable.OlympiadByAgeTable.OlympiadTable.OlympiadName,
                        n.OlympiadPlanTable.OlimpiadByAgeAndLevelTable.OlympiadByAgeTable.SchoolStageTable.SchoolStage,
                        n.OlympiadPlanTable.OlimpiadByAgeAndLevelTable.LevelTable.OlympiadLevel,
                        n.OlympiadPlanTable.OlimpiadByAgeAndLevelTable.OlympiadByAgeTable.OlympiadTable.Team,
                        date = n.OlympiadPlanTable.Date,
                        n.StudentTable.StudentSurname,
                        n.StudentTable.StudentName,
                        n.StudentTable.StudentMiddle,
                        n.Class,
                        n.Scores,
                        n.ResultTable.Result,
                        n.RewardTable.Reward,
                        n.Absent
                    });

                search = search
               .Where(n => n.OlympiadPlanId == index);

                if (search.Count() != 0)
                {
                    questions = false;
                    miAccounting_Click(null, null);
                    dgvAccounting.DataSource = search
                       .OrderByDescending(n => new { n.date, n.OlympiadName, n.Scores }).ToList();
                    InterfaceDataGridViewForAccounting();
                }
                else
                {
                    MessageBox.Show("Записей по данной олимпиаде нет", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                miResult_Click(null, null);
            }
        }

        private void cbNameStudent_TextChanged(object sender, EventArgs e)
        {
            if (cbNameStudent.SelectedIndex == -1)
            {
                regex = new Regex(@"^" + cbNameStudent.Text + @"(\w*)", RegexOptions.IgnoreCase);
                cbNameStudent.Items.Clear();
                for (int i = 0; i < name.Count; i++)
                {
                    if (regex.IsMatch(name[i]))
                    {
                        cbNameStudent.Items.Add(name[i]);
                    }
                }
                cbNameStudent.SelectionStart = cbNameStudent.Text.Length;

                if (cbNameStudent.Text.Length == 0)
                {
                    FilterForAccounting();
                }
            }
        }

        private void miReport_Click(object sender, EventArgs e)
        {
            FormReport report = new FormReport();
            report.ShowDialog();
        }

        private void btnDroppingPlan_Click(object sender, EventArgs e)
        {
            DateSetting(dtpStartDate, dtpEndDate);
        }

        private void dtpStartDate_ValueChanged(object sender, EventArgs e)
        {
            Filter();
        }

        private void dtpEndDate_ValueChanged(object sender, EventArgs e)
        {
            Filter();
        }

        int olympiadIndex;

        private void miRequest_Click(object sender, EventArgs e)
        {
            int indexDate;

            if (table == "plan")
            {
                indexDate = 5;
                olympiadIndex = (int)(dgvAccounting.Rows[dgvAccounting.CurrentRow.Index]
                .Cells[0].Value);

            }
            else
            {
                indexDate = 7;
                olympiadIndex = (int)(dgvAccounting.Rows[dgvAccounting.CurrentRow.Index]
                .Cells[1].Value);
            }

            if (Convert.ToDateTime(dgvAccounting.Rows[dgvAccounting.CurrentRow.Index]
                .Cells[indexDate].Value) > DateTime.Today)
            {
                if (db.RegistrationOfParticipantsTables.FirstOrDefault(n => n.OlympiadPlanId == olympiadIndex) != null)
                {
                    if (MessageBox.Show("Заявка формируется сразу для всех зарегистрированных на " +
                    "данную олимпиаду учеников. Продолжить?", "Внимание!",
                    MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
                    {
                        export = false;
                        saveWord.FileName = "Заявка на олимпиаду";
                        saveWord.ShowDialog();
                    }
                }
                else
                {
                    MessageBox.Show("Нет учеников, зарегистрированных на данную олимпиаду.",
                        "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                MessageBox.Show("Данная олимпиада уже прошла\r\nВыберите ожидаемую олимпиаду.", "Внимание!",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void miCancelParticipation_Click(object sender, EventArgs e)
        {
            var index = int.Parse(dgvAccounting[0, dgvAccounting.CurrentRow.Index].Value.ToString());
            if (db.RegistrationOfParticipantsTables.FirstOrDefault(n => n.RegistrationOfParticipantsId == index 
            && n.OlympiadPlanTable.Date > DateTime.Now) != null)
            {
                if (MessageBox.Show("Вы уверены, что хотите Удалить запись?", "Внимание!", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    var participation = db.RegistrationOfParticipantsTables.FirstOrDefault(n => n.RegistrationOfParticipantsId == index);
                    db.RegistrationOfParticipantsTables.Remove(participation);
                    db.SaveChanges();
                    questions = false;
                    miAccounting_Click(null, null);
                }
            }
            else
            {
                MessageBox.Show("Данная олимпиада прошла, " +
                    "\r\nнельзя редактировать список участников", "Ошибка!", 
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void FormAccountingForParticipation_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (MessageBox.Show("Вы уверены, что хотите выйти?", "Внимание!",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes)
            {
                e.Cancel = true;
            }
        }
    }
}