﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParticipationInOlympiads
{
    public partial class FormChangesPlan : Form
    {
        public FormChangesPlan()
        {
            InitializeComponent();
            dtpPlan.MinDate = DateTime.Today;
            dtpPlan.MaxDate = DateTime.Now.AddYears(2);
        }

        dbRegistrationOfParticipationInOlympiadsEntities db = new dbRegistrationOfParticipationInOlympiadsEntities();
        public int index = -1;

        private void FormChangesPlan_Load(object sender, EventArgs e)
        {
            cbNameOlympiad.DataSource = db.OlympiadTables.OrderBy(n => n.OlympiadName).Select(n => n.OlympiadName).ToList();
            cbSchoolStage.DataSource = db.SchoolStageTables.Select(n => n.SchoolStage).ToList();
            cbLevel.DataSource = db.LevelTables.Select(n => n.OlympiadLevel).ToList();
            if (this.Text == "Изменение данных об олимпиаде")
            {
                if (db.OlympiadPlanTables.FirstOrDefault(n => n.OlympiadPlanId == index).Date > DateTime.Today)
                {
                    dtpPlan.MinDate = DateTime.Today.AddDays(1);
                }
                pnlNewOlympiad.Visible = false;
                cbNameOlympiad.Text = db.OlympiadPlanTables.FirstOrDefault(n => n.OlympiadPlanId == index)
                    .OlimpiadByAgeAndLevelTable.OlympiadByAgeTable.OlympiadTable.OlympiadName;
                cbSchoolStage.Text = db.OlympiadPlanTables.FirstOrDefault(n => n.OlympiadPlanId == index)
                    .OlimpiadByAgeAndLevelTable.OlympiadByAgeTable.SchoolStageTable.SchoolStage;
                cbLevel.Text = db.OlympiadPlanTables.FirstOrDefault(n => n.OlympiadPlanId == index)
                    .OlimpiadByAgeAndLevelTable.LevelTable.OlympiadLevel;
                dtpPlan.Value = db.OlympiadPlanTables.FirstOrDefault(n => n.OlympiadPlanId == index).Date;
            }
            else
            {
                cbSchoolStage.SelectedIndex = -1;
                cbLevel.SelectedIndex = -1;
                dtpPlan.Value = DateTime.Today.AddDays(1);
                dtpPlan.MinDate = DateTime.Today.AddDays(1);
            }
        }

        public void Check()
        {
            var olympiadAgeLevel = db.OlimpiadByAgeAndLevelTables
                            .Where(n => n.OlympiadByAgeTable.OlympiadTable.OlympiadName == cbNameOlympiad.Text
                            && n.OlympiadByAgeTable.SchoolStageTable.SchoolStage == cbSchoolStage.Text
                            && n.LevelTable.OlympiadLevel == cbLevel.Text).ToList();

            if (olympiadAgeLevel.Count == 0)
            {
                var olympiadAge = db.OlympiadByAgeTables
                    .Where(n => n.OlympiadTable.OlympiadName == cbNameOlympiad.Text
                    && n.SchoolStageTable.SchoolStage == cbSchoolStage.Text).ToList();

                if (olympiadAge.Count == 0)
                {
                    OlympiadByAgeTable olympiadByAge = new OlympiadByAgeTable
                    {
                        OlympiadId = db.OlympiadTables.FirstOrDefault(n => n.OlympiadName == cbNameOlympiad.Text)
                    .OlympiadId,
                        SchoolStageId = db.SchoolStageTables.FirstOrDefault(n => n.SchoolStage == cbSchoolStage.Text).SchoolStageId,
                    };
                    db.OlympiadByAgeTables.Add(olympiadByAge);
                    db.SaveChanges();
                }

                OlimpiadByAgeAndLevelTable olimpiadByAgeAndLevel = new OlimpiadByAgeAndLevelTable
                {
                    OlympiadByAgeId = db.OlympiadByAgeTables
                .FirstOrDefault(n => n.OlympiadTable.OlympiadName == cbNameOlympiad.Text
                && n.SchoolStageTable.SchoolStage == cbSchoolStage.Text).OlympiadByAgeId,
                    OlympiadLevelId = db.LevelTables.FirstOrDefault(n => n.OlympiadLevel == cbLevel.Text).OlympiadLevelId,
                };
                db.OlimpiadByAgeAndLevelTables.Add(olimpiadByAgeAndLevel);
                db.SaveChanges();
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(cbLevel.Text) && !String.IsNullOrEmpty(cbSchoolStage.Text))
            {
                if (db.LevelTables.FirstOrDefault(n => n.OlympiadLevel == cbLevel.Text) == null)
                {
                    LevelTable levelTable = new LevelTable
                    {
                        OlympiadLevel = cbLevel.Text
                    };
                    db.LevelTables.Add(levelTable);
                    db.SaveChanges();
                }

                if (db.SchoolStageTables.FirstOrDefault(n => n.SchoolStage == cbSchoolStage.Text) == null)
                {
                    SchoolStageTable schoolStage = new SchoolStageTable
                    {
                        SchoolStage = cbSchoolStage.Text
                    };
                    db.SchoolStageTables.Add(schoolStage);
                    db.SaveChanges();
                }

                if (this.Text == "Регистрация олимпиады")
                {
                    var olympiadAgeLevelPlan = db.OlympiadPlanTables
                           .FirstOrDefault(n => n.OlimpiadByAgeAndLevelTable.OlympiadByAgeTable.OlympiadTable.OlympiadName == cbNameOlympiad.Text
                           && n.OlimpiadByAgeAndLevelTable.OlympiadByAgeTable.SchoolStageTable.SchoolStage == cbSchoolStage.Text
                           && n.OlimpiadByAgeAndLevelTable.LevelTable.OlympiadLevel == cbLevel.Text
                           && n.Date == dtpPlan.Value.Date);

                    if (olympiadAgeLevelPlan != null)
                    {
                        MessageBox.Show("Олимпиада уже зарегестрирована ранее", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else
                    {
                        Check();
                        OlympiadPlanTable olympiadPlan = new OlympiadPlanTable
                        {
                            OlympiadId = db.OlimpiadByAgeAndLevelTables
                            .FirstOrDefault(n => n.OlympiadByAgeTable.OlympiadTable.OlympiadName == cbNameOlympiad.Text
                            && n.OlympiadByAgeTable.SchoolStageTable.SchoolStage == cbSchoolStage.Text
                            && n.LevelTable.OlympiadLevel == cbLevel.Text).Id,
                            Date = dtpPlan.Value
                        };
                        db.OlympiadPlanTables.Add(olympiadPlan);
                        db.SaveChanges();
                        MessageBox.Show("Данные добавлены в таблицу.", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
                else
                {
                    Check();

                    var olympiadPlan = db.OlympiadPlanTables.FirstOrDefault(n => n.OlympiadPlanId == index);
                    olympiadPlan.OlympiadId = db.OlimpiadByAgeAndLevelTables
                        .FirstOrDefault(n => n.OlympiadByAgeTable.OlympiadTable.OlympiadName == cbNameOlympiad.Text
                        && n.OlympiadByAgeTable.SchoolStageTable.SchoolStage == cbSchoolStage.Text
                        && n.LevelTable.OlympiadLevel == cbLevel.Text).Id;
                    olympiadPlan.Date = dtpPlan.Value;
                    db.SaveChanges();
                    MessageBox.Show("Данные успешно изменены.", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                this.DialogResult = DialogResult.Yes;
                exit = true;
                this.Close();
            }
            else
            {
                MessageBox.Show("Заполните все поля.", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public bool exit = false;

        private void FormChangesPlan_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!exit)
            {
                if ((MessageBox.Show("Вы уверены, что хотите отменить действие?", "Внимание!",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question)) == DialogResult.Yes)
                {
                    exit = true;
                }
                else
                {
                    e.Cancel = true;
                }
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            FormNewOlympiad newOlympiad = new FormNewOlympiad();
            newOlympiad.ShowDialog();
            cbNameOlympiad.DataSource = db.OlympiadTables.OrderBy(n => n.OlympiadName).Select(n => n.OlympiadName).ToList();
            if (newOlympiad.DialogResult == DialogResult.OK)
            {
                cbNameOlympiad.SelectedItem = db.OlympiadTables
                    .FirstOrDefault(k => k.OlympiadId == (db.OlympiadTables.Max(n => n.OlympiadId))).OlympiadName;
            }
        }
    }
}