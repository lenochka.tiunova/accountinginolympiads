﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParticipationInOlympiads
{
    public partial class FormNewStudent : Form
    {
        public FormNewStudent()
        {
            InitializeComponent();
        }

        dbRegistrationOfParticipationInOlympiadsEntities db = new dbRegistrationOfParticipationInOlympiadsEntities();
        public int index = -1;

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(tbName.Text)
                    && !String.IsNullOrEmpty(tbSurname.Text)
                    && !String.IsNullOrEmpty(tbMiddle.Text))
            {
                if (db.ClassTables.FirstOrDefault(n => n.Class == (int)nudClass.Value 
                && n.DateAdmission.Year == DateTime.Now.Year - (int)nudClass.Value) == null)
                {
                    ClassTable classTable = new ClassTable
                    {
                        Class = (byte)nudClass.Value, 
                        DateAdmission = new DateTime(DateTime.Now.Year - (int)nudClass.Value, 09, 01)
                    };
                    db.ClassTables.Add(classTable);
                    db.SaveChanges();
                }

                if (this.Text == "Добавление ученика")
                {
                    StudentTable student = new StudentTable
                    {
                        StudentSurname = tbSurname.Text,
                        StudentName = tbName.Text,
                        StudentMiddle = tbMiddle.Text,
                        StudentClass = db.ClassTables.FirstOrDefault(n => n.Class == (int)nudClass.Value
                            && n.DateAdmission.Year == DateTime.Now.Year - (int)nudClass.Value).ClassId
                    };
                    db.StudentTables.Add(student);
                    db.SaveChanges();
                    MessageBox.Show("Ученик добавлен.", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else if (this.Text == "Изменение информации об ученике")
                {
                    var student = db.StudentTables.FirstOrDefault(n => n.StudentId == index);
                    student.StudentSurname = tbSurname.Text;
                    student.StudentName = tbName.Text;
                    student.StudentMiddle = tbMiddle.Text;
                    student.StudentClass = db.ClassTables.FirstOrDefault(n => n.Class == (int)nudClass.Value
                            && n.DateAdmission.Year == DateTime.Now.Year - (int)nudClass.Value).ClassId;
                    db.SaveChanges();
                    MessageBox.Show("Данные успешно изменены.", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                this.DialogResult = DialogResult.Yes;
                exit = true;
                this.Close();
            }
            else
            {
                MessageBox.Show("Заполните все поля", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        bool exit = false;

        private void FormNewStudent_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!exit)
            {
                if ((MessageBox.Show("Вы уверены, что хотите отменить действие?", "Внимание!",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question)) == DialogResult.Yes)
                {
                    exit = true;
                }
                else
                {
                    e.Cancel = true;
                }
            }
        }

        private void FormNewStudent_Load(object sender, EventArgs e)
        {
            if (this.Text == "Изменение информации об ученике")
            {
                var student = db.StudentTables.FirstOrDefault(n => n.StudentId == index);
                tbSurname.Text = student.StudentSurname;
                tbName.Text = student.StudentName;
                tbMiddle.Text = student.StudentMiddle;
                nudClass.Value = student.ClassTable.Class;
            }
        }

        public void OnlyLetter(object sender, KeyPressEventArgs e, int cursor, TextBox textBox)
        {
            if (e.KeyChar == '-' && !textBox.Text.Contains("-"))
            {
                if (cursor == 0)
                {
                    e.Handled = true;
                }
            }
            else if(!Char.IsLetter(e.KeyChar) && e.KeyChar != Convert.ToChar(8))
            {
                e.Handled = true;
            }
        }

        private void tbSurname_KeyPress(object sender, KeyPressEventArgs e)
        {
            int cursor = tbSurname.SelectionStart;
            OnlyLetter(sender, e, cursor, tbSurname);
        }

        private void tbName_KeyPress(object sender, KeyPressEventArgs e)
        {
            int cursor = tbName.SelectionStart;
            OnlyLetter(sender, e, cursor, tbName);
        }

        private void tbMiddle_KeyPress(object sender, KeyPressEventArgs e)
        {
            int cursor = tbMiddle.SelectionStart;
            OnlyLetter(sender, e, cursor, tbMiddle);
        }

        private void Errors(TextBox textBox)
        {
            if (textBox.Text[0] == '-')
            {
                textBox.Text = textBox.Text.Remove(0, 1);
            }
            else if (textBox.Text[textBox.Text.Length - 1] == '-')
            {
                textBox.Text = textBox.Text.Remove(textBox.Text.Length - 1, 1);
            }
            if (textBox.Text == "-")
            {
                textBox.Text = "";
            }
        }

        private void tbSurname_Leave(object sender, EventArgs e)
        {
            if (tbSurname.Text != "")
            {
                Errors(tbSurname);
            }
        }

        private void tbName_Leave(object sender, EventArgs e)
        {
            if (tbName.Text != "")
            {
                Errors(tbName);
            }
        }

        private void tbMiddle_Leave(object sender, EventArgs e)
        {
            if (tbMiddle.Text != "")
            {
                Errors(tbMiddle);
            }
        }
    }
}
