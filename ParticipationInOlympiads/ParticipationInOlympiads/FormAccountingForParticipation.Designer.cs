﻿
namespace ParticipationInOlympiads
{
    partial class FormAccountingForParticipation
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormAccountingForParticipation));
            this.menuParticipationInOlympiads = new System.Windows.Forms.MenuStrip();
            this.miFile = new System.Windows.Forms.ToolStripMenuItem();
            this.miOpen = new System.Windows.Forms.ToolStripMenuItem();
            this.miPlan = new System.Windows.Forms.ToolStripMenuItem();
            this.miAccounting = new System.Windows.Forms.ToolStripMenuItem();
            this.miEditing = new System.Windows.Forms.ToolStripMenuItem();
            this.miEditData = new System.Windows.Forms.ToolStripMenuItem();
            this.miAddOlympiad = new System.Windows.Forms.ToolStripMenuItem();
            this.miAddStudent = new System.Windows.Forms.ToolStripMenuItem();
            this.miViewInfo = new System.Windows.Forms.ToolStripMenuItem();
            this.miCancel = new System.Windows.Forms.ToolStripMenuItem();
            this.miResult = new System.Windows.Forms.ToolStripMenuItem();
            this.miCancelParticipation = new System.Windows.Forms.ToolStripMenuItem();
            this.miExport = new System.Windows.Forms.ToolStripMenuItem();
            this.miExcel = new System.Windows.Forms.ToolStripMenuItem();
            this.miWord = new System.Windows.Forms.ToolStripMenuItem();
            this.miReport = new System.Windows.Forms.ToolStripMenuItem();
            this.miRequest = new System.Windows.Forms.ToolStripMenuItem();
            this.dgvAccounting = new System.Windows.Forms.DataGridView();
            this.olympiadPlanTableBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.tbFind = new System.Windows.Forms.TextBox();
            this.pnlPlanOlympiad = new System.Windows.Forms.Panel();
            this.gbPeriodPlan = new System.Windows.Forms.GroupBox();
            this.btnDroppingPlan = new System.Windows.Forms.Button();
            this.lblEndDate = new System.Windows.Forms.Label();
            this.dtpStartDate = new System.Windows.Forms.DateTimePicker();
            this.dtpEndDate = new System.Windows.Forms.DateTimePicker();
            this.lblStartDate = new System.Windows.Forms.Label();
            this.btnDroppAll = new System.Windows.Forms.Button();
            this.gbSubject = new System.Windows.Forms.GroupBox();
            this.cbSubject = new System.Windows.Forms.ComboBox();
            this.gbFindLevel = new System.Windows.Forms.GroupBox();
            this.clbLevel = new System.Windows.Forms.CheckedListBox();
            this.gbFindStage = new System.Windows.Forms.GroupBox();
            this.clbStage = new System.Windows.Forms.CheckedListBox();
            this.gbFindName = new System.Windows.Forms.GroupBox();
            this.pnlAccounting = new System.Windows.Forms.Panel();
            this.btnClearAll = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cbSubjectForRegisration = new System.Windows.Forms.ComboBox();
            this.gbPeriod = new System.Windows.Forms.GroupBox();
            this.btnDropping = new System.Windows.Forms.Button();
            this.lblEnd = new System.Windows.Forms.Label();
            this.dtpStart = new System.Windows.Forms.DateTimePicker();
            this.dtpEnd = new System.Windows.Forms.DateTimePicker();
            this.lblStart = new System.Windows.Forms.Label();
            this.gbStudent = new System.Windows.Forms.GroupBox();
            this.cbNameStudent = new System.Windows.Forms.ComboBox();
            this.gbLevel = new System.Windows.Forms.GroupBox();
            this.clblLevelOlympiad = new System.Windows.Forms.CheckedListBox();
            this.gbStage = new System.Windows.Forms.GroupBox();
            this.clbClass = new System.Windows.Forms.CheckedListBox();
            this.clbStageOlympiad = new System.Windows.Forms.CheckedListBox();
            this.rbStage = new System.Windows.Forms.RadioButton();
            this.rbClass = new System.Windows.Forms.RadioButton();
            this.gbName = new System.Windows.Forms.GroupBox();
            this.tbName = new System.Windows.Forms.TextBox();
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.lblCount = new System.Windows.Forms.ToolStripStatusLabel();
            this.saveExcel = new System.Windows.Forms.SaveFileDialog();
            this.saveWord = new System.Windows.Forms.SaveFileDialog();
            this.menuParticipationInOlympiads.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAccounting)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.olympiadPlanTableBindingSource)).BeginInit();
            this.pnlPlanOlympiad.SuspendLayout();
            this.gbPeriodPlan.SuspendLayout();
            this.gbSubject.SuspendLayout();
            this.gbFindLevel.SuspendLayout();
            this.gbFindStage.SuspendLayout();
            this.gbFindName.SuspendLayout();
            this.pnlAccounting.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.gbPeriod.SuspendLayout();
            this.gbStudent.SuspendLayout();
            this.gbLevel.SuspendLayout();
            this.gbStage.SuspendLayout();
            this.gbName.SuspendLayout();
            this.statusStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuParticipationInOlympiads
            // 
            this.menuParticipationInOlympiads.BackColor = System.Drawing.Color.RosyBrown;
            this.menuParticipationInOlympiads.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miFile,
            this.miEditing,
            this.miExport,
            this.miReport,
            this.miRequest});
            this.menuParticipationInOlympiads.Location = new System.Drawing.Point(0, 0);
            this.menuParticipationInOlympiads.Name = "menuParticipationInOlympiads";
            this.menuParticipationInOlympiads.Size = new System.Drawing.Size(1073, 24);
            this.menuParticipationInOlympiads.TabIndex = 0;
            this.menuParticipationInOlympiads.Text = "menuStrip1";
            // 
            // miFile
            // 
            this.miFile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miOpen});
            this.miFile.Name = "miFile";
            this.miFile.Size = new System.Drawing.Size(48, 20);
            this.miFile.Text = "Файл";
            // 
            // miOpen
            // 
            this.miOpen.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miPlan,
            this.miAccounting});
            this.miOpen.Name = "miOpen";
            this.miOpen.Size = new System.Drawing.Size(121, 22);
            this.miOpen.Text = "Открыть";
            // 
            // miPlan
            // 
            this.miPlan.Name = "miPlan";
            this.miPlan.Size = new System.Drawing.Size(230, 22);
            this.miPlan.Text = "План проведения олимпиад";
            this.miPlan.Click += new System.EventHandler(this.miPlan_Click);
            // 
            // miAccounting
            // 
            this.miAccounting.Name = "miAccounting";
            this.miAccounting.Size = new System.Drawing.Size(230, 22);
            this.miAccounting.Text = "Учет участников олимпиад";
            this.miAccounting.Click += new System.EventHandler(this.miAccounting_Click);
            // 
            // miEditing
            // 
            this.miEditing.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miEditData,
            this.miAddOlympiad,
            this.miAddStudent,
            this.miViewInfo,
            this.miCancel,
            this.miResult,
            this.miCancelParticipation});
            this.miEditing.Name = "miEditing";
            this.miEditing.Size = new System.Drawing.Size(108, 20);
            this.miEditing.Text = "Редактирование";
            // 
            // miEditData
            // 
            this.miEditData.Name = "miEditData";
            this.miEditData.Size = new System.Drawing.Size(302, 22);
            this.miEditData.Text = "Изменение характеристик олимпиады";
            this.miEditData.Visible = false;
            this.miEditData.Click += new System.EventHandler(this.miEditDate_Click);
            // 
            // miAddOlympiad
            // 
            this.miAddOlympiad.Name = "miAddOlympiad";
            this.miAddOlympiad.Size = new System.Drawing.Size(302, 22);
            this.miAddOlympiad.Text = "Регистрация олимпиады";
            this.miAddOlympiad.Visible = false;
            this.miAddOlympiad.Click += new System.EventHandler(this.miAddOlympiad_Click);
            // 
            // miAddStudent
            // 
            this.miAddStudent.Name = "miAddStudent";
            this.miAddStudent.Size = new System.Drawing.Size(302, 22);
            this.miAddStudent.Text = "Регистрация участника";
            this.miAddStudent.Visible = false;
            this.miAddStudent.Click += new System.EventHandler(this.miAddStudent_Click);
            // 
            // miViewInfo
            // 
            this.miViewInfo.Name = "miViewInfo";
            this.miViewInfo.Size = new System.Drawing.Size(302, 22);
            this.miViewInfo.Text = "Посмотреть информацию об олимпиаде";
            this.miViewInfo.Visible = false;
            this.miViewInfo.Click += new System.EventHandler(this.miViewInfo_Click);
            // 
            // miCancel
            // 
            this.miCancel.Name = "miCancel";
            this.miCancel.Size = new System.Drawing.Size(302, 22);
            this.miCancel.Text = "Отменить планирование";
            this.miCancel.Visible = false;
            this.miCancel.Click += new System.EventHandler(this.miCancel_Click);
            // 
            // miResult
            // 
            this.miResult.Name = "miResult";
            this.miResult.Size = new System.Drawing.Size(302, 22);
            this.miResult.Text = "Фиксация результата";
            this.miResult.Click += new System.EventHandler(this.miResult_Click);
            // 
            // miCancelParticipation
            // 
            this.miCancelParticipation.Name = "miCancelParticipation";
            this.miCancelParticipation.Size = new System.Drawing.Size(302, 22);
            this.miCancelParticipation.Text = "Отменить регистрацию участника";
            this.miCancelParticipation.Click += new System.EventHandler(this.miCancelParticipation_Click);
            // 
            // miExport
            // 
            this.miExport.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miExcel,
            this.miWord});
            this.miExport.Name = "miExport";
            this.miExport.Size = new System.Drawing.Size(64, 20);
            this.miExport.Text = "Экспорт";
            // 
            // miExcel
            // 
            this.miExcel.Name = "miExcel";
            this.miExcel.Size = new System.Drawing.Size(180, 22);
            this.miExcel.Text = "В Excel";
            this.miExcel.Click += new System.EventHandler(this.miExcel_Click);
            // 
            // miWord
            // 
            this.miWord.Name = "miWord";
            this.miWord.Size = new System.Drawing.Size(180, 22);
            this.miWord.Text = "В Word";
            this.miWord.Click += new System.EventHandler(this.miWord_Click);
            // 
            // miReport
            // 
            this.miReport.Name = "miReport";
            this.miReport.Size = new System.Drawing.Size(51, 20);
            this.miReport.Text = "Отчет";
            this.miReport.Click += new System.EventHandler(this.miReport_Click);
            // 
            // miRequest
            // 
            this.miRequest.BackColor = System.Drawing.Color.Thistle;
            this.miRequest.Name = "miRequest";
            this.miRequest.Size = new System.Drawing.Size(222, 20);
            this.miRequest.Text = "Сформировать заявку на олимпиаду";
            this.miRequest.Click += new System.EventHandler(this.miRequest_Click);
            // 
            // dgvAccounting
            // 
            this.dgvAccounting.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvAccounting.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dgvAccounting.BackgroundColor = System.Drawing.Color.Linen;
            this.dgvAccounting.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvAccounting.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvAccounting.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.NullValue = null;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvAccounting.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvAccounting.Location = new System.Drawing.Point(287, 27);
            this.dgvAccounting.MultiSelect = false;
            this.dgvAccounting.Name = "dgvAccounting";
            this.dgvAccounting.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvAccounting.Size = new System.Drawing.Size(786, 720);
            this.dgvAccounting.TabIndex = 1;
            this.dgvAccounting.TabStop = false;
            this.dgvAccounting.DataSourceChanged += new System.EventHandler(this.dgvAccounting_DataSourceChanged);
            this.dgvAccounting.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dgvAccounting_CellFormatting);
            this.dgvAccounting.DoubleClick += new System.EventHandler(this.dgvAccounting_DoubleClick);
            // 
            // olympiadPlanTableBindingSource
            // 
            this.olympiadPlanTableBindingSource.DataMember = "OlympiadPlanTable";
            // 
            // tbFind
            // 
            this.tbFind.Location = new System.Drawing.Point(15, 37);
            this.tbFind.Name = "tbFind";
            this.tbFind.Size = new System.Drawing.Size(221, 26);
            this.tbFind.TabIndex = 2;
            this.tbFind.TextChanged += new System.EventHandler(this.tbFind_TextChanged);
            // 
            // pnlPlanOlympiad
            // 
            this.pnlPlanOlympiad.Controls.Add(this.gbPeriodPlan);
            this.pnlPlanOlympiad.Controls.Add(this.btnDroppAll);
            this.pnlPlanOlympiad.Controls.Add(this.gbSubject);
            this.pnlPlanOlympiad.Controls.Add(this.gbFindLevel);
            this.pnlPlanOlympiad.Controls.Add(this.gbFindStage);
            this.pnlPlanOlympiad.Controls.Add(this.gbFindName);
            this.pnlPlanOlympiad.Location = new System.Drawing.Point(4, 27);
            this.pnlPlanOlympiad.Name = "pnlPlanOlympiad";
            this.pnlPlanOlympiad.Size = new System.Drawing.Size(277, 695);
            this.pnlPlanOlympiad.TabIndex = 4;
            // 
            // gbPeriodPlan
            // 
            this.gbPeriodPlan.Controls.Add(this.btnDroppingPlan);
            this.gbPeriodPlan.Controls.Add(this.lblEndDate);
            this.gbPeriodPlan.Controls.Add(this.dtpStartDate);
            this.gbPeriodPlan.Controls.Add(this.dtpEndDate);
            this.gbPeriodPlan.Controls.Add(this.lblStartDate);
            this.gbPeriodPlan.Font = new System.Drawing.Font("Calibri", 11.25F);
            this.gbPeriodPlan.Location = new System.Drawing.Point(13, 515);
            this.gbPeriodPlan.Name = "gbPeriodPlan";
            this.gbPeriodPlan.Size = new System.Drawing.Size(251, 123);
            this.gbPeriodPlan.TabIndex = 16;
            this.gbPeriodPlan.TabStop = false;
            this.gbPeriodPlan.Text = "Выберите период";
            // 
            // btnDroppingPlan
            // 
            this.btnDroppingPlan.Location = new System.Drawing.Point(66, 88);
            this.btnDroppingPlan.Name = "btnDroppingPlan";
            this.btnDroppingPlan.Size = new System.Drawing.Size(93, 25);
            this.btnDroppingPlan.TabIndex = 11;
            this.btnDroppingPlan.Text = "Сброс";
            this.btnDroppingPlan.UseVisualStyleBackColor = true;
            this.btnDroppingPlan.Click += new System.EventHandler(this.btnDroppingPlan_Click);
            // 
            // lblEndDate
            // 
            this.lblEndDate.AutoSize = true;
            this.lblEndDate.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblEndDate.Location = new System.Drawing.Point(4, 58);
            this.lblEndDate.Name = "lblEndDate";
            this.lblEndDate.Size = new System.Drawing.Size(100, 18);
            this.lblEndDate.TabIndex = 2;
            this.lblEndDate.Text = "Конечная дата";
            // 
            // dtpStartDate
            // 
            this.dtpStartDate.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.dtpStartDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpStartDate.Location = new System.Drawing.Point(108, 25);
            this.dtpStartDate.Name = "dtpStartDate";
            this.dtpStartDate.Size = new System.Drawing.Size(137, 23);
            this.dtpStartDate.TabIndex = 7;
            this.dtpStartDate.ValueChanged += new System.EventHandler(this.dtpStartDate_ValueChanged);
            // 
            // dtpEndDate
            // 
            this.dtpEndDate.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.dtpEndDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpEndDate.Location = new System.Drawing.Point(108, 57);
            this.dtpEndDate.Name = "dtpEndDate";
            this.dtpEndDate.Size = new System.Drawing.Size(137, 23);
            this.dtpEndDate.TabIndex = 8;
            this.dtpEndDate.ValueChanged += new System.EventHandler(this.dtpEndDate_ValueChanged);
            // 
            // lblStartDate
            // 
            this.lblStartDate.AutoSize = true;
            this.lblStartDate.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblStartDate.Location = new System.Drawing.Point(4, 26);
            this.lblStartDate.Name = "lblStartDate";
            this.lblStartDate.Size = new System.Drawing.Size(106, 18);
            this.lblStartDate.TabIndex = 1;
            this.lblStartDate.Text = "Начальная дата";
            // 
            // btnDroppAll
            // 
            this.btnDroppAll.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnDroppAll.Location = new System.Drawing.Point(157, 644);
            this.btnDroppAll.Name = "btnDroppAll";
            this.btnDroppAll.Size = new System.Drawing.Size(107, 25);
            this.btnDroppAll.TabIndex = 15;
            this.btnDroppAll.Text = "Очистить всё";
            this.btnDroppAll.UseVisualStyleBackColor = true;
            this.btnDroppAll.Click += new System.EventHandler(this.btnDroppAll_Click);
            // 
            // gbSubject
            // 
            this.gbSubject.Controls.Add(this.cbSubject);
            this.gbSubject.Font = new System.Drawing.Font("Calibri", 11.25F);
            this.gbSubject.Location = new System.Drawing.Point(13, 421);
            this.gbSubject.Name = "gbSubject";
            this.gbSubject.Size = new System.Drawing.Size(251, 82);
            this.gbSubject.TabIndex = 9;
            this.gbSubject.TabStop = false;
            this.gbSubject.Text = "Выберите предмет";
            // 
            // cbSubject
            // 
            this.cbSubject.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbSubject.FormattingEnabled = true;
            this.cbSubject.Location = new System.Drawing.Point(15, 34);
            this.cbSubject.Name = "cbSubject";
            this.cbSubject.Size = new System.Drawing.Size(221, 26);
            this.cbSubject.TabIndex = 0;
            this.cbSubject.SelectedIndexChanged += new System.EventHandler(this.cbSubject_SelectedIndexChanged);
            // 
            // gbFindLevel
            // 
            this.gbFindLevel.Controls.Add(this.clbLevel);
            this.gbFindLevel.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.gbFindLevel.Location = new System.Drawing.Point(13, 266);
            this.gbFindLevel.Name = "gbFindLevel";
            this.gbFindLevel.Size = new System.Drawing.Size(251, 140);
            this.gbFindLevel.TabIndex = 8;
            this.gbFindLevel.TabStop = false;
            this.gbFindLevel.Text = "Выберите уровень олимпиады";
            // 
            // clbLevel
            // 
            this.clbLevel.CheckOnClick = true;
            this.clbLevel.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.clbLevel.FormattingEnabled = true;
            this.clbLevel.Location = new System.Drawing.Point(15, 25);
            this.clbLevel.Name = "clbLevel";
            this.clbLevel.Size = new System.Drawing.Size(221, 109);
            this.clbLevel.TabIndex = 6;
            this.clbLevel.SelectedIndexChanged += new System.EventHandler(this.clbLevel_SelectedIndexChanged);
            // 
            // gbFindStage
            // 
            this.gbFindStage.Controls.Add(this.clbStage);
            this.gbFindStage.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.gbFindStage.Location = new System.Drawing.Point(13, 125);
            this.gbFindStage.Name = "gbFindStage";
            this.gbFindStage.Size = new System.Drawing.Size(251, 128);
            this.gbFindStage.TabIndex = 7;
            this.gbFindStage.TabStop = false;
            this.gbFindStage.Text = "Выберите область классов";
            // 
            // clbStage
            // 
            this.clbStage.CheckOnClick = true;
            this.clbStage.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.clbStage.FormattingEnabled = true;
            this.clbStage.Location = new System.Drawing.Point(15, 25);
            this.clbStage.Name = "clbStage";
            this.clbStage.Size = new System.Drawing.Size(221, 88);
            this.clbStage.TabIndex = 5;
            this.clbStage.SelectedIndexChanged += new System.EventHandler(this.clbStage_SelectedIndexChanged);
            // 
            // gbFindName
            // 
            this.gbFindName.Controls.Add(this.tbFind);
            this.gbFindName.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.gbFindName.Location = new System.Drawing.Point(13, 13);
            this.gbFindName.Name = "gbFindName";
            this.gbFindName.Size = new System.Drawing.Size(251, 100);
            this.gbFindName.TabIndex = 4;
            this.gbFindName.TabStop = false;
            this.gbFindName.Text = "Введите название олимпиады";
            // 
            // pnlAccounting
            // 
            this.pnlAccounting.BackColor = System.Drawing.Color.Linen;
            this.pnlAccounting.Controls.Add(this.btnClearAll);
            this.pnlAccounting.Controls.Add(this.groupBox1);
            this.pnlAccounting.Controls.Add(this.gbPeriod);
            this.pnlAccounting.Controls.Add(this.gbStudent);
            this.pnlAccounting.Controls.Add(this.gbLevel);
            this.pnlAccounting.Controls.Add(this.gbStage);
            this.pnlAccounting.Controls.Add(this.gbName);
            this.pnlAccounting.Location = new System.Drawing.Point(4, 27);
            this.pnlAccounting.Name = "pnlAccounting";
            this.pnlAccounting.Size = new System.Drawing.Size(277, 695);
            this.pnlAccounting.TabIndex = 5;
            this.pnlAccounting.Visible = false;
            // 
            // btnClearAll
            // 
            this.btnClearAll.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnClearAll.Location = new System.Drawing.Point(154, 637);
            this.btnClearAll.Name = "btnClearAll";
            this.btnClearAll.Size = new System.Drawing.Size(107, 25);
            this.btnClearAll.TabIndex = 10;
            this.btnClearAll.Text = "Очистить всё";
            this.btnClearAll.UseVisualStyleBackColor = true;
            this.btnClearAll.Click += new System.EventHandler(this.btnClearAll_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cbSubjectForRegisration);
            this.groupBox1.Font = new System.Drawing.Font("Calibri", 11.25F);
            this.groupBox1.Location = new System.Drawing.Point(13, 567);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(251, 64);
            this.groupBox1.TabIndex = 9;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Выберите предмет";
            // 
            // cbSubjectForRegisration
            // 
            this.cbSubjectForRegisration.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbSubjectForRegisration.FormattingEnabled = true;
            this.cbSubjectForRegisration.Location = new System.Drawing.Point(15, 23);
            this.cbSubjectForRegisration.Name = "cbSubjectForRegisration";
            this.cbSubjectForRegisration.Size = new System.Drawing.Size(221, 26);
            this.cbSubjectForRegisration.TabIndex = 9;
            this.cbSubjectForRegisration.SelectedIndexChanged += new System.EventHandler(this.cbSubjectForRegisration_SelectedIndexChanged);
            // 
            // gbPeriod
            // 
            this.gbPeriod.Controls.Add(this.btnDropping);
            this.gbPeriod.Controls.Add(this.lblEnd);
            this.gbPeriod.Controls.Add(this.dtpStart);
            this.gbPeriod.Controls.Add(this.dtpEnd);
            this.gbPeriod.Controls.Add(this.lblStart);
            this.gbPeriod.Font = new System.Drawing.Font("Calibri", 11.25F);
            this.gbPeriod.Location = new System.Drawing.Point(13, 443);
            this.gbPeriod.Name = "gbPeriod";
            this.gbPeriod.Size = new System.Drawing.Size(251, 123);
            this.gbPeriod.TabIndex = 7;
            this.gbPeriod.TabStop = false;
            this.gbPeriod.Text = "Выберите период";
            // 
            // btnDropping
            // 
            this.btnDropping.Location = new System.Drawing.Point(66, 88);
            this.btnDropping.Name = "btnDropping";
            this.btnDropping.Size = new System.Drawing.Size(93, 25);
            this.btnDropping.TabIndex = 11;
            this.btnDropping.Text = "Сброс";
            this.btnDropping.UseVisualStyleBackColor = true;
            this.btnDropping.Click += new System.EventHandler(this.btnDropping_Click);
            // 
            // lblEnd
            // 
            this.lblEnd.AutoSize = true;
            this.lblEnd.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblEnd.Location = new System.Drawing.Point(4, 58);
            this.lblEnd.Name = "lblEnd";
            this.lblEnd.Size = new System.Drawing.Size(100, 18);
            this.lblEnd.TabIndex = 2;
            this.lblEnd.Text = "Конечная дата";
            // 
            // dtpStart
            // 
            this.dtpStart.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.dtpStart.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpStart.Location = new System.Drawing.Point(108, 25);
            this.dtpStart.Name = "dtpStart";
            this.dtpStart.Size = new System.Drawing.Size(137, 23);
            this.dtpStart.TabIndex = 7;
            this.dtpStart.ValueChanged += new System.EventHandler(this.dtpStart_ValueChanged);
            // 
            // dtpEnd
            // 
            this.dtpEnd.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.dtpEnd.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpEnd.Location = new System.Drawing.Point(108, 57);
            this.dtpEnd.Name = "dtpEnd";
            this.dtpEnd.Size = new System.Drawing.Size(137, 23);
            this.dtpEnd.TabIndex = 8;
            this.dtpEnd.ValueChanged += new System.EventHandler(this.dtpEnd_ValueChanged);
            // 
            // lblStart
            // 
            this.lblStart.AutoSize = true;
            this.lblStart.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblStart.Location = new System.Drawing.Point(4, 26);
            this.lblStart.Name = "lblStart";
            this.lblStart.Size = new System.Drawing.Size(106, 18);
            this.lblStart.TabIndex = 1;
            this.lblStart.Text = "Начальная дата";
            // 
            // gbStudent
            // 
            this.gbStudent.Controls.Add(this.cbNameStudent);
            this.gbStudent.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.gbStudent.Location = new System.Drawing.Point(13, 379);
            this.gbStudent.Name = "gbStudent";
            this.gbStudent.Size = new System.Drawing.Size(251, 63);
            this.gbStudent.TabIndex = 6;
            this.gbStudent.TabStop = false;
            this.gbStudent.Text = "Выберите ученика";
            // 
            // cbNameStudent
            // 
            this.cbNameStudent.FormattingEnabled = true;
            this.cbNameStudent.Location = new System.Drawing.Point(3, 25);
            this.cbNameStudent.Name = "cbNameStudent";
            this.cbNameStudent.Size = new System.Drawing.Size(245, 26);
            this.cbNameStudent.TabIndex = 6;
            this.cbNameStudent.SelectedIndexChanged += new System.EventHandler(this.cbNameStudent_SelectedIndexChanged);
            this.cbNameStudent.TextChanged += new System.EventHandler(this.cbNameStudent_TextChanged);
            // 
            // gbLevel
            // 
            this.gbLevel.Controls.Add(this.clblLevelOlympiad);
            this.gbLevel.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.gbLevel.Location = new System.Drawing.Point(13, 279);
            this.gbLevel.Name = "gbLevel";
            this.gbLevel.Size = new System.Drawing.Size(251, 101);
            this.gbLevel.TabIndex = 5;
            this.gbLevel.TabStop = false;
            this.gbLevel.Text = "Выберите уровень олимпиады";
            // 
            // clblLevelOlympiad
            // 
            this.clblLevelOlympiad.CheckOnClick = true;
            this.clblLevelOlympiad.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.clblLevelOlympiad.FormattingEnabled = true;
            this.clblLevelOlympiad.Location = new System.Drawing.Point(15, 25);
            this.clblLevelOlympiad.Name = "clblLevelOlympiad";
            this.clblLevelOlympiad.Size = new System.Drawing.Size(221, 67);
            this.clblLevelOlympiad.TabIndex = 5;
            this.clblLevelOlympiad.SelectedIndexChanged += new System.EventHandler(this.clblLevelOlympiad_SelectedIndexChanged);
            // 
            // gbStage
            // 
            this.gbStage.Controls.Add(this.clbClass);
            this.gbStage.Controls.Add(this.clbStageOlympiad);
            this.gbStage.Controls.Add(this.rbStage);
            this.gbStage.Controls.Add(this.rbClass);
            this.gbStage.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.gbStage.Location = new System.Drawing.Point(13, 64);
            this.gbStage.Name = "gbStage";
            this.gbStage.Size = new System.Drawing.Size(251, 215);
            this.gbStage.TabIndex = 1;
            this.gbStage.TabStop = false;
            this.gbStage.Text = "Выберите область классов";
            // 
            // clbClass
            // 
            this.clbClass.CheckOnClick = true;
            this.clbClass.ColumnWidth = 45;
            this.clbClass.Enabled = false;
            this.clbClass.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.clbClass.FormattingEnabled = true;
            this.clbClass.Location = new System.Drawing.Point(15, 137);
            this.clbClass.MultiColumn = true;
            this.clbClass.Name = "clbClass";
            this.clbClass.Size = new System.Drawing.Size(221, 67);
            this.clbClass.TabIndex = 4;
            this.clbClass.SelectedIndexChanged += new System.EventHandler(this.clbClass_SelectedIndexChanged);
            // 
            // clbStageOlympiad
            // 
            this.clbStageOlympiad.CheckOnClick = true;
            this.clbStageOlympiad.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.clbStageOlympiad.FormattingEnabled = true;
            this.clbStageOlympiad.Location = new System.Drawing.Point(15, 42);
            this.clbStageOlympiad.Name = "clbStageOlympiad";
            this.clbStageOlympiad.Size = new System.Drawing.Size(221, 67);
            this.clbStageOlympiad.TabIndex = 2;
            this.clbStageOlympiad.SelectedIndexChanged += new System.EventHandler(this.clbStageOlympiad_SelectedIndexChanged);
            // 
            // rbStage
            // 
            this.rbStage.AutoSize = true;
            this.rbStage.Checked = true;
            this.rbStage.Location = new System.Drawing.Point(15, 16);
            this.rbStage.Name = "rbStage";
            this.rbStage.Size = new System.Drawing.Size(64, 22);
            this.rbStage.TabIndex = 1;
            this.rbStage.TabStop = true;
            this.rbStage.Text = "Звено";
            this.rbStage.UseVisualStyleBackColor = true;
            this.rbStage.CheckedChanged += new System.EventHandler(this.rbStage_CheckedChanged);
            // 
            // rbClass
            // 
            this.rbClass.AutoSize = true;
            this.rbClass.Location = new System.Drawing.Point(15, 113);
            this.rbClass.Name = "rbClass";
            this.rbClass.Size = new System.Drawing.Size(81, 22);
            this.rbClass.TabIndex = 3;
            this.rbClass.Text = "Класс(ы)";
            this.rbClass.UseVisualStyleBackColor = true;
            this.rbClass.CheckedChanged += new System.EventHandler(this.rbClass_CheckedChanged);
            // 
            // gbName
            // 
            this.gbName.Controls.Add(this.tbName);
            this.gbName.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.gbName.Location = new System.Drawing.Point(13, 4);
            this.gbName.Name = "gbName";
            this.gbName.Size = new System.Drawing.Size(251, 60);
            this.gbName.TabIndex = 0;
            this.gbName.TabStop = false;
            this.gbName.Text = "Введите название олимпиады";
            // 
            // tbName
            // 
            this.tbName.Location = new System.Drawing.Point(15, 25);
            this.tbName.Name = "tbName";
            this.tbName.Size = new System.Drawing.Size(221, 26);
            this.tbName.TabIndex = 0;
            this.tbName.TextChanged += new System.EventHandler(this.tbName_TextChanged);
            // 
            // statusStrip
            // 
            this.statusStrip.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.statusStrip.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.statusStrip.Dock = System.Windows.Forms.DockStyle.None;
            this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblCount});
            this.statusStrip.Location = new System.Drawing.Point(0, 725);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.Size = new System.Drawing.Size(139, 22);
            this.statusStrip.TabIndex = 6;
            this.statusStrip.Text = "statusStrip";
            // 
            // lblCount
            // 
            this.lblCount.Name = "lblCount";
            this.lblCount.Size = new System.Drawing.Size(122, 17);
            this.lblCount.Text = "Количество записей:";
            // 
            // saveExcel
            // 
            this.saveExcel.Filter = "Книга Excel (*.xlsx)|*.xlsx";
            this.saveExcel.FileOk += new System.ComponentModel.CancelEventHandler(this.saveExcel_FileOk);
            // 
            // saveWord
            // 
            this.saveWord.Filter = "Документ Microsoft Word (*.docx)|*.docx";
            this.saveWord.FileOk += new System.ComponentModel.CancelEventHandler(this.saveWord_FileOk);
            // 
            // FormAccountingForParticipation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Linen;
            this.ClientSize = new System.Drawing.Size(1073, 749);
            this.Controls.Add(this.statusStrip);
            this.Controls.Add(this.pnlAccounting);
            this.Controls.Add(this.pnlPlanOlympiad);
            this.Controls.Add(this.dgvAccounting);
            this.Controls.Add(this.menuParticipationInOlympiads);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuParticipationInOlympiads;
            this.Name = "FormAccountingForParticipation";
            this.Text = "Учет участия в олимпиадах";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormAccountingForParticipation_FormClosing);
            this.menuParticipationInOlympiads.ResumeLayout(false);
            this.menuParticipationInOlympiads.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAccounting)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.olympiadPlanTableBindingSource)).EndInit();
            this.pnlPlanOlympiad.ResumeLayout(false);
            this.gbPeriodPlan.ResumeLayout(false);
            this.gbPeriodPlan.PerformLayout();
            this.gbSubject.ResumeLayout(false);
            this.gbFindLevel.ResumeLayout(false);
            this.gbFindStage.ResumeLayout(false);
            this.gbFindName.ResumeLayout(false);
            this.gbFindName.PerformLayout();
            this.pnlAccounting.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.gbPeriod.ResumeLayout(false);
            this.gbPeriod.PerformLayout();
            this.gbStudent.ResumeLayout(false);
            this.gbLevel.ResumeLayout(false);
            this.gbStage.ResumeLayout(false);
            this.gbStage.PerformLayout();
            this.gbName.ResumeLayout(false);
            this.gbName.PerformLayout();
            this.statusStrip.ResumeLayout(false);
            this.statusStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuParticipationInOlympiads;
        private System.Windows.Forms.ToolStripMenuItem miFile;
        private System.Windows.Forms.DataGridView dgvAccounting;
        private System.Windows.Forms.ToolStripMenuItem miEditing;
        private System.Windows.Forms.ToolStripMenuItem miEditData;
        private System.Windows.Forms.ToolStripMenuItem miAddOlympiad;
        private System.Windows.Forms.TextBox tbFind;
        private System.Windows.Forms.ToolStripMenuItem miOpen;
        private System.Windows.Forms.ToolStripMenuItem miPlan;
        private System.Windows.Forms.ToolStripMenuItem miAccounting;
        private System.Windows.Forms.ToolStripMenuItem miAddStudent;
        private System.Windows.Forms.Panel pnlPlanOlympiad;
        private System.Windows.Forms.GroupBox gbFindName;
        private System.Windows.Forms.CheckedListBox clbLevel;
        private System.Windows.Forms.CheckedListBox clbStage;
        private System.Windows.Forms.GroupBox gbFindLevel;
        private System.Windows.Forms.GroupBox gbFindStage;
        private System.Windows.Forms.Panel pnlAccounting;
        private System.Windows.Forms.GroupBox gbLevel;
        private System.Windows.Forms.CheckedListBox clblLevelOlympiad;
        private System.Windows.Forms.GroupBox gbStage;
        private System.Windows.Forms.CheckedListBox clbStageOlympiad;
        private System.Windows.Forms.GroupBox gbName;
        private System.Windows.Forms.TextBox tbName;
        private System.Windows.Forms.GroupBox gbStudent;
        private System.Windows.Forms.ComboBox cbNameStudent;
        private System.Windows.Forms.GroupBox gbPeriod;
        private System.Windows.Forms.Button btnDropping;
        private System.Windows.Forms.Label lblEnd;
        private System.Windows.Forms.DateTimePicker dtpStart;
        private System.Windows.Forms.DateTimePicker dtpEnd;
        private System.Windows.Forms.Label lblStart;
        private System.Windows.Forms.CheckedListBox clbClass;
        private System.Windows.Forms.RadioButton rbClass;
        private System.Windows.Forms.RadioButton rbStage;
        private System.Windows.Forms.StatusStrip statusStrip;
        private System.Windows.Forms.ToolStripStatusLabel lblCount;
        private System.Windows.Forms.GroupBox gbSubject;
        private System.Windows.Forms.ComboBox cbSubject;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox cbSubjectForRegisration;
        private System.Windows.Forms.ToolStripMenuItem miExport;
        private System.Windows.Forms.SaveFileDialog saveExcel;
        private System.Windows.Forms.ToolStripMenuItem miViewInfo;
        private System.Windows.Forms.Button btnDroppAll;
        private System.Windows.Forms.Button btnClearAll;
        private System.Windows.Forms.ToolStripMenuItem miExcel;
        private System.Windows.Forms.ToolStripMenuItem miWord;
        private System.Windows.Forms.SaveFileDialog saveWord;
        private System.Windows.Forms.ToolStripMenuItem miCancel;
        private System.Windows.Forms.BindingSource olympiadPlanTableBindingSource;
        private System.Windows.Forms.ToolStripMenuItem miResult;
        private System.Windows.Forms.ToolStripMenuItem miReport;
        private System.Windows.Forms.GroupBox gbPeriodPlan;
        private System.Windows.Forms.Button btnDroppingPlan;
        private System.Windows.Forms.Label lblEndDate;
        private System.Windows.Forms.DateTimePicker dtpStartDate;
        private System.Windows.Forms.DateTimePicker dtpEndDate;
        private System.Windows.Forms.Label lblStartDate;
        private System.Windows.Forms.ToolStripMenuItem miRequest;
        private System.Windows.Forms.ToolStripMenuItem miCancelParticipation;
    }
}

