USE [master]
GO
/****** Object:  Database [dbRegistrationOfParticipationInOlympiads]    Script Date: 30.04.2021 2:40:30 ******/
CREATE DATABASE [dbRegistrationOfParticipationInOlympiads]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'dbRegistrationOfParticipationInOlympiads', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPREESS\MSSQL\DATA\dbRegistrationOfParticipationInOlympiads.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'dbRegistrationOfParticipationInOlympiads_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPREESS\MSSQL\DATA\dbRegistrationOfParticipationInOlympiads_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [dbRegistrationOfParticipationInOlympiads] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [dbRegistrationOfParticipationInOlympiads].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [dbRegistrationOfParticipationInOlympiads] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [dbRegistrationOfParticipationInOlympiads] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [dbRegistrationOfParticipationInOlympiads] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [dbRegistrationOfParticipationInOlympiads] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [dbRegistrationOfParticipationInOlympiads] SET ARITHABORT OFF 
GO
ALTER DATABASE [dbRegistrationOfParticipationInOlympiads] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [dbRegistrationOfParticipationInOlympiads] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [dbRegistrationOfParticipationInOlympiads] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [dbRegistrationOfParticipationInOlympiads] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [dbRegistrationOfParticipationInOlympiads] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [dbRegistrationOfParticipationInOlympiads] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [dbRegistrationOfParticipationInOlympiads] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [dbRegistrationOfParticipationInOlympiads] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [dbRegistrationOfParticipationInOlympiads] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [dbRegistrationOfParticipationInOlympiads] SET  DISABLE_BROKER 
GO
ALTER DATABASE [dbRegistrationOfParticipationInOlympiads] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [dbRegistrationOfParticipationInOlympiads] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [dbRegistrationOfParticipationInOlympiads] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [dbRegistrationOfParticipationInOlympiads] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [dbRegistrationOfParticipationInOlympiads] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [dbRegistrationOfParticipationInOlympiads] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [dbRegistrationOfParticipationInOlympiads] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [dbRegistrationOfParticipationInOlympiads] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [dbRegistrationOfParticipationInOlympiads] SET  MULTI_USER 
GO
ALTER DATABASE [dbRegistrationOfParticipationInOlympiads] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [dbRegistrationOfParticipationInOlympiads] SET DB_CHAINING OFF 
GO
ALTER DATABASE [dbRegistrationOfParticipationInOlympiads] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [dbRegistrationOfParticipationInOlympiads] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [dbRegistrationOfParticipationInOlympiads] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [dbRegistrationOfParticipationInOlympiads] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
ALTER DATABASE [dbRegistrationOfParticipationInOlympiads] SET QUERY_STORE = OFF
GO
USE [dbRegistrationOfParticipationInOlympiads]
GO
/****** Object:  Table [dbo].[ClassTable]    Script Date: 30.04.2021 2:40:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ClassTable](
	[ClassId] [int] IDENTITY(1,1) NOT NULL,
	[Class] [tinyint] NOT NULL,
	[DateAdmission] [date] NOT NULL,
 CONSTRAINT [PK_ClassTable] PRIMARY KEY CLUSTERED 
(
	[ClassId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[LevelTable]    Script Date: 30.04.2021 2:40:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LevelTable](
	[OlympiadLevelId] [int] IDENTITY(1,1) NOT NULL,
	[OlympiadLevel] [varchar](30) NOT NULL,
 CONSTRAINT [PK_OlympiadLevelTable] PRIMARY KEY CLUSTERED 
(
	[OlympiadLevelId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OlimpiadByAgeAndLevelTable]    Script Date: 30.04.2021 2:40:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OlimpiadByAgeAndLevelTable](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[OlympiadByAgeId] [int] NOT NULL,
	[OlympiadLevelId] [int] NOT NULL,
 CONSTRAINT [PK_OlimpiadByAgeAndLevelTable] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OlympiadByAgeTable]    Script Date: 30.04.2021 2:40:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OlympiadByAgeTable](
	[OlympiadByAgeId] [int] IDENTITY(1,1) NOT NULL,
	[OlympiadId] [int] NOT NULL,
	[SchoolStageId] [int] NOT NULL,
 CONSTRAINT [PK_OlympiadByAgeTable] PRIMARY KEY CLUSTERED 
(
	[OlympiadByAgeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OlympiadPlanTable]    Script Date: 30.04.2021 2:40:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OlympiadPlanTable](
	[OlympiadPlanId] [int] IDENTITY(1,1) NOT NULL,
	[OlympiadId] [int] NOT NULL,
	[Date] [date] NOT NULL,
 CONSTRAINT [PK_OlympiadPlanTable] PRIMARY KEY CLUSTERED 
(
	[OlympiadPlanId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OlympiadTable]    Script Date: 30.04.2021 2:40:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OlympiadTable](
	[OlympiadId] [int] IDENTITY(1,1) NOT NULL,
	[OlympiadName] [varchar](50) NOT NULL,
	[SubjectId] [int] NOT NULL,
	[Description] [varchar](max) NOT NULL,
	[Team] [bit] NOT NULL,
 CONSTRAINT [PK_OlympiadTable] PRIMARY KEY CLUSTERED 
(
	[OlympiadId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[RegistrationOfParticipantsTable]    Script Date: 30.04.2021 2:40:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RegistrationOfParticipantsTable](
	[RegistrationOfParticipantsId] [int] IDENTITY(1,1) NOT NULL,
	[StudentId] [int] NOT NULL,
	[Class] [int] NOT NULL,
	[Scores] [decimal](5, 2) NULL,
	[OlympiadPlanId] [int] NOT NULL,
	[ResultId] [int] NULL,
	[RewardId] [int] NULL,
	[Absent] [bit] NULL,
 CONSTRAINT [PK_RegistrationOfParticipantsTable] PRIMARY KEY CLUSTERED 
(
	[RegistrationOfParticipantsId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ResultTable]    Script Date: 30.04.2021 2:40:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ResultTable](
	[ResultId] [int] IDENTITY(1,1) NOT NULL,
	[Result] [varchar](30) NOT NULL,
 CONSTRAINT [PK_ResultTable] PRIMARY KEY CLUSTERED 
(
	[ResultId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[RewardTable]    Script Date: 30.04.2021 2:40:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RewardTable](
	[RewardId] [int] IDENTITY(1,1) NOT NULL,
	[Reward] [varchar](50) NOT NULL,
 CONSTRAINT [PK_RewardTable] PRIMARY KEY CLUSTERED 
(
	[RewardId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SchoolStageTable]    Script Date: 30.04.2021 2:40:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SchoolStageTable](
	[SchoolStageId] [int] IDENTITY(1,1) NOT NULL,
	[SchoolStage] [varchar](30) NOT NULL,
 CONSTRAINT [PK_SchoolStageTable] PRIMARY KEY CLUSTERED 
(
	[SchoolStageId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[StudentTable]    Script Date: 30.04.2021 2:40:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[StudentTable](
	[StudentId] [int] IDENTITY(1,1) NOT NULL,
	[StudentSurname] [varchar](30) NOT NULL,
	[StudentName] [varchar](30) NOT NULL,
	[StudentMiddle] [varchar](30) NOT NULL,
	[StudentClass] [int] NOT NULL,
 CONSTRAINT [PK_StudentTable] PRIMARY KEY CLUSTERED 
(
	[StudentId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SubjectTable]    Script Date: 30.04.2021 2:40:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SubjectTable](
	[SubjectId] [int] IDENTITY(1,1) NOT NULL,
	[Subject] [varchar](30) NOT NULL,
 CONSTRAINT [PK_SubjectTable] PRIMARY KEY CLUSTERED 
(
	[SubjectId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[ClassTable] ON 

INSERT [dbo].[ClassTable] ([ClassId], [Class], [DateAdmission]) VALUES (1, 1, CAST(N'2020-09-01' AS Date))
INSERT [dbo].[ClassTable] ([ClassId], [Class], [DateAdmission]) VALUES (2, 2, CAST(N'2019-09-01' AS Date))
INSERT [dbo].[ClassTable] ([ClassId], [Class], [DateAdmission]) VALUES (3, 3, CAST(N'2018-09-01' AS Date))
INSERT [dbo].[ClassTable] ([ClassId], [Class], [DateAdmission]) VALUES (4, 4, CAST(N'2017-09-01' AS Date))
INSERT [dbo].[ClassTable] ([ClassId], [Class], [DateAdmission]) VALUES (5, 5, CAST(N'2016-09-01' AS Date))
INSERT [dbo].[ClassTable] ([ClassId], [Class], [DateAdmission]) VALUES (6, 6, CAST(N'2015-09-01' AS Date))
INSERT [dbo].[ClassTable] ([ClassId], [Class], [DateAdmission]) VALUES (7, 7, CAST(N'2014-09-01' AS Date))
INSERT [dbo].[ClassTable] ([ClassId], [Class], [DateAdmission]) VALUES (8, 8, CAST(N'2013-09-01' AS Date))
INSERT [dbo].[ClassTable] ([ClassId], [Class], [DateAdmission]) VALUES (9, 9, CAST(N'2012-09-01' AS Date))
INSERT [dbo].[ClassTable] ([ClassId], [Class], [DateAdmission]) VALUES (10, 10, CAST(N'2011-09-01' AS Date))
INSERT [dbo].[ClassTable] ([ClassId], [Class], [DateAdmission]) VALUES (11, 11, CAST(N'2010-09-01' AS Date))
INSERT [dbo].[ClassTable] ([ClassId], [Class], [DateAdmission]) VALUES (12, 12, CAST(N'2009-09-01' AS Date))
SET IDENTITY_INSERT [dbo].[ClassTable] OFF
GO
SET IDENTITY_INSERT [dbo].[LevelTable] ON 

INSERT [dbo].[LevelTable] ([OlympiadLevelId], [OlympiadLevel]) VALUES (1, N'Внутришкольный')
INSERT [dbo].[LevelTable] ([OlympiadLevelId], [OlympiadLevel]) VALUES (2, N'Муниципальный')
INSERT [dbo].[LevelTable] ([OlympiadLevelId], [OlympiadLevel]) VALUES (3, N'Региональный')
INSERT [dbo].[LevelTable] ([OlympiadLevelId], [OlympiadLevel]) VALUES (4, N'Государственный')
SET IDENTITY_INSERT [dbo].[LevelTable] OFF
GO
SET IDENTITY_INSERT [dbo].[OlimpiadByAgeAndLevelTable] ON 

INSERT [dbo].[OlimpiadByAgeAndLevelTable] ([Id], [OlympiadByAgeId], [OlympiadLevelId]) VALUES (1, 1, 4)
INSERT [dbo].[OlimpiadByAgeAndLevelTable] ([Id], [OlympiadByAgeId], [OlympiadLevelId]) VALUES (2, 2, 4)
INSERT [dbo].[OlimpiadByAgeAndLevelTable] ([Id], [OlympiadByAgeId], [OlympiadLevelId]) VALUES (3, 3, 4)
INSERT [dbo].[OlimpiadByAgeAndLevelTable] ([Id], [OlympiadByAgeId], [OlympiadLevelId]) VALUES (4, 4, 4)
INSERT [dbo].[OlimpiadByAgeAndLevelTable] ([Id], [OlympiadByAgeId], [OlympiadLevelId]) VALUES (5, 5, 4)
INSERT [dbo].[OlimpiadByAgeAndLevelTable] ([Id], [OlympiadByAgeId], [OlympiadLevelId]) VALUES (6, 7, 4)
INSERT [dbo].[OlimpiadByAgeAndLevelTable] ([Id], [OlympiadByAgeId], [OlympiadLevelId]) VALUES (7, 8, 4)
INSERT [dbo].[OlimpiadByAgeAndLevelTable] ([Id], [OlympiadByAgeId], [OlympiadLevelId]) VALUES (8, 9, 4)
INSERT [dbo].[OlimpiadByAgeAndLevelTable] ([Id], [OlympiadByAgeId], [OlympiadLevelId]) VALUES (9, 11, 4)
INSERT [dbo].[OlimpiadByAgeAndLevelTable] ([Id], [OlympiadByAgeId], [OlympiadLevelId]) VALUES (10, 12, 1)
INSERT [dbo].[OlimpiadByAgeAndLevelTable] ([Id], [OlympiadByAgeId], [OlympiadLevelId]) VALUES (11, 12, 2)
INSERT [dbo].[OlimpiadByAgeAndLevelTable] ([Id], [OlympiadByAgeId], [OlympiadLevelId]) VALUES (12, 13, 1)
INSERT [dbo].[OlimpiadByAgeAndLevelTable] ([Id], [OlympiadByAgeId], [OlympiadLevelId]) VALUES (13, 13, 2)
INSERT [dbo].[OlimpiadByAgeAndLevelTable] ([Id], [OlympiadByAgeId], [OlympiadLevelId]) VALUES (14, 13, 3)
INSERT [dbo].[OlimpiadByAgeAndLevelTable] ([Id], [OlympiadByAgeId], [OlympiadLevelId]) VALUES (15, 13, 4)
INSERT [dbo].[OlimpiadByAgeAndLevelTable] ([Id], [OlympiadByAgeId], [OlympiadLevelId]) VALUES (16, 14, 1)
INSERT [dbo].[OlimpiadByAgeAndLevelTable] ([Id], [OlympiadByAgeId], [OlympiadLevelId]) VALUES (17, 14, 2)
INSERT [dbo].[OlimpiadByAgeAndLevelTable] ([Id], [OlympiadByAgeId], [OlympiadLevelId]) VALUES (18, 15, 1)
INSERT [dbo].[OlimpiadByAgeAndLevelTable] ([Id], [OlympiadByAgeId], [OlympiadLevelId]) VALUES (19, 15, 2)
INSERT [dbo].[OlimpiadByAgeAndLevelTable] ([Id], [OlympiadByAgeId], [OlympiadLevelId]) VALUES (20, 15, 3)
INSERT [dbo].[OlimpiadByAgeAndLevelTable] ([Id], [OlympiadByAgeId], [OlympiadLevelId]) VALUES (21, 15, 4)
INSERT [dbo].[OlimpiadByAgeAndLevelTable] ([Id], [OlympiadByAgeId], [OlympiadLevelId]) VALUES (22, 16, 1)
INSERT [dbo].[OlimpiadByAgeAndLevelTable] ([Id], [OlympiadByAgeId], [OlympiadLevelId]) VALUES (23, 16, 2)
INSERT [dbo].[OlimpiadByAgeAndLevelTable] ([Id], [OlympiadByAgeId], [OlympiadLevelId]) VALUES (24, 17, 1)
INSERT [dbo].[OlimpiadByAgeAndLevelTable] ([Id], [OlympiadByAgeId], [OlympiadLevelId]) VALUES (25, 17, 2)
INSERT [dbo].[OlimpiadByAgeAndLevelTable] ([Id], [OlympiadByAgeId], [OlympiadLevelId]) VALUES (26, 17, 3)
INSERT [dbo].[OlimpiadByAgeAndLevelTable] ([Id], [OlympiadByAgeId], [OlympiadLevelId]) VALUES (27, 17, 4)
INSERT [dbo].[OlimpiadByAgeAndLevelTable] ([Id], [OlympiadByAgeId], [OlympiadLevelId]) VALUES (28, 19, 2)
INSERT [dbo].[OlimpiadByAgeAndLevelTable] ([Id], [OlympiadByAgeId], [OlympiadLevelId]) VALUES (29, 19, 3)
INSERT [dbo].[OlimpiadByAgeAndLevelTable] ([Id], [OlympiadByAgeId], [OlympiadLevelId]) VALUES (30, 19, 4)
INSERT [dbo].[OlimpiadByAgeAndLevelTable] ([Id], [OlympiadByAgeId], [OlympiadLevelId]) VALUES (41, 20, 1)
INSERT [dbo].[OlimpiadByAgeAndLevelTable] ([Id], [OlympiadByAgeId], [OlympiadLevelId]) VALUES (42, 21, 1)
INSERT [dbo].[OlimpiadByAgeAndLevelTable] ([Id], [OlympiadByAgeId], [OlympiadLevelId]) VALUES (43, 22, 2)
INSERT [dbo].[OlimpiadByAgeAndLevelTable] ([Id], [OlympiadByAgeId], [OlympiadLevelId]) VALUES (44, 23, 2)
INSERT [dbo].[OlimpiadByAgeAndLevelTable] ([Id], [OlympiadByAgeId], [OlympiadLevelId]) VALUES (45, 24, 1)
INSERT [dbo].[OlimpiadByAgeAndLevelTable] ([Id], [OlympiadByAgeId], [OlympiadLevelId]) VALUES (46, 25, 1)
INSERT [dbo].[OlimpiadByAgeAndLevelTable] ([Id], [OlympiadByAgeId], [OlympiadLevelId]) VALUES (48, 1, 2)
INSERT [dbo].[OlimpiadByAgeAndLevelTable] ([Id], [OlympiadByAgeId], [OlympiadLevelId]) VALUES (49, 1, 1)
INSERT [dbo].[OlimpiadByAgeAndLevelTable] ([Id], [OlympiadByAgeId], [OlympiadLevelId]) VALUES (50, 16, 3)
INSERT [dbo].[OlimpiadByAgeAndLevelTable] ([Id], [OlympiadByAgeId], [OlympiadLevelId]) VALUES (1046, 1025, 1)
INSERT [dbo].[OlimpiadByAgeAndLevelTable] ([Id], [OlympiadByAgeId], [OlympiadLevelId]) VALUES (1047, 1025, 2)
INSERT [dbo].[OlimpiadByAgeAndLevelTable] ([Id], [OlympiadByAgeId], [OlympiadLevelId]) VALUES (1048, 19, 1)
INSERT [dbo].[OlimpiadByAgeAndLevelTable] ([Id], [OlympiadByAgeId], [OlympiadLevelId]) VALUES (1049, 1026, 4)
INSERT [dbo].[OlimpiadByAgeAndLevelTable] ([Id], [OlympiadByAgeId], [OlympiadLevelId]) VALUES (1050, 1025, 3)
INSERT [dbo].[OlimpiadByAgeAndLevelTable] ([Id], [OlympiadByAgeId], [OlympiadLevelId]) VALUES (1051, 1025, 4)
INSERT [dbo].[OlimpiadByAgeAndLevelTable] ([Id], [OlympiadByAgeId], [OlympiadLevelId]) VALUES (1052, 5, 3)
SET IDENTITY_INSERT [dbo].[OlimpiadByAgeAndLevelTable] OFF
GO
SET IDENTITY_INSERT [dbo].[OlympiadByAgeTable] ON 

INSERT [dbo].[OlympiadByAgeTable] ([OlympiadByAgeId], [OlympiadId], [SchoolStageId]) VALUES (1, 1, 1)
INSERT [dbo].[OlympiadByAgeTable] ([OlympiadByAgeId], [OlympiadId], [SchoolStageId]) VALUES (2, 1, 2)
INSERT [dbo].[OlympiadByAgeTable] ([OlympiadByAgeId], [OlympiadId], [SchoolStageId]) VALUES (3, 1, 3)
INSERT [dbo].[OlympiadByAgeTable] ([OlympiadByAgeId], [OlympiadId], [SchoolStageId]) VALUES (4, 2, 1)
INSERT [dbo].[OlympiadByAgeTable] ([OlympiadByAgeId], [OlympiadId], [SchoolStageId]) VALUES (5, 2, 2)
INSERT [dbo].[OlympiadByAgeTable] ([OlympiadByAgeId], [OlympiadId], [SchoolStageId]) VALUES (7, 2, 3)
INSERT [dbo].[OlympiadByAgeTable] ([OlympiadByAgeId], [OlympiadId], [SchoolStageId]) VALUES (8, 3, 1)
INSERT [dbo].[OlympiadByAgeTable] ([OlympiadByAgeId], [OlympiadId], [SchoolStageId]) VALUES (9, 3, 2)
INSERT [dbo].[OlympiadByAgeTable] ([OlympiadByAgeId], [OlympiadId], [SchoolStageId]) VALUES (11, 3, 3)
INSERT [dbo].[OlympiadByAgeTable] ([OlympiadByAgeId], [OlympiadId], [SchoolStageId]) VALUES (12, 4, 2)
INSERT [dbo].[OlympiadByAgeTable] ([OlympiadByAgeId], [OlympiadId], [SchoolStageId]) VALUES (13, 4, 3)
INSERT [dbo].[OlympiadByAgeTable] ([OlympiadByAgeId], [OlympiadId], [SchoolStageId]) VALUES (14, 5, 2)
INSERT [dbo].[OlympiadByAgeTable] ([OlympiadByAgeId], [OlympiadId], [SchoolStageId]) VALUES (15, 5, 3)
INSERT [dbo].[OlympiadByAgeTable] ([OlympiadByAgeId], [OlympiadId], [SchoolStageId]) VALUES (16, 6, 2)
INSERT [dbo].[OlympiadByAgeTable] ([OlympiadByAgeId], [OlympiadId], [SchoolStageId]) VALUES (17, 6, 3)
INSERT [dbo].[OlympiadByAgeTable] ([OlympiadByAgeId], [OlympiadId], [SchoolStageId]) VALUES (19, 8, 2)
INSERT [dbo].[OlympiadByAgeTable] ([OlympiadByAgeId], [OlympiadId], [SchoolStageId]) VALUES (20, 4, 1)
INSERT [dbo].[OlympiadByAgeTable] ([OlympiadByAgeId], [OlympiadId], [SchoolStageId]) VALUES (21, 5, 1)
INSERT [dbo].[OlympiadByAgeTable] ([OlympiadByAgeId], [OlympiadId], [SchoolStageId]) VALUES (22, 10, 1)
INSERT [dbo].[OlympiadByAgeTable] ([OlympiadByAgeId], [OlympiadId], [SchoolStageId]) VALUES (23, 6, 1)
INSERT [dbo].[OlympiadByAgeTable] ([OlympiadByAgeId], [OlympiadId], [SchoolStageId]) VALUES (24, 8, 3)
INSERT [dbo].[OlympiadByAgeTable] ([OlympiadByAgeId], [OlympiadId], [SchoolStageId]) VALUES (25, 11, 3)
INSERT [dbo].[OlympiadByAgeTable] ([OlympiadByAgeId], [OlympiadId], [SchoolStageId]) VALUES (1025, 11, 2)
INSERT [dbo].[OlympiadByAgeTable] ([OlympiadByAgeId], [OlympiadId], [SchoolStageId]) VALUES (1026, 1011, 3)
SET IDENTITY_INSERT [dbo].[OlympiadByAgeTable] OFF
GO
SET IDENTITY_INSERT [dbo].[OlympiadPlanTable] ON 

INSERT [dbo].[OlympiadPlanTable] ([OlympiadPlanId], [OlympiadId], [Date]) VALUES (1, 1, CAST(N'2021-03-09' AS Date))
INSERT [dbo].[OlympiadPlanTable] ([OlympiadPlanId], [OlympiadId], [Date]) VALUES (2, 2, CAST(N'2021-03-09' AS Date))
INSERT [dbo].[OlympiadPlanTable] ([OlympiadPlanId], [OlympiadId], [Date]) VALUES (4, 4, CAST(N'2021-03-09' AS Date))
INSERT [dbo].[OlympiadPlanTable] ([OlympiadPlanId], [OlympiadId], [Date]) VALUES (5, 5, CAST(N'2021-03-09' AS Date))
INSERT [dbo].[OlympiadPlanTable] ([OlympiadPlanId], [OlympiadId], [Date]) VALUES (7, 7, CAST(N'2021-03-09' AS Date))
INSERT [dbo].[OlympiadPlanTable] ([OlympiadPlanId], [OlympiadId], [Date]) VALUES (8, 8, CAST(N'2021-03-09' AS Date))
INSERT [dbo].[OlympiadPlanTable] ([OlympiadPlanId], [OlympiadId], [Date]) VALUES (11, 11, CAST(N'2021-03-25' AS Date))
INSERT [dbo].[OlympiadPlanTable] ([OlympiadPlanId], [OlympiadId], [Date]) VALUES (13, 13, CAST(N'2021-03-25' AS Date))
INSERT [dbo].[OlympiadPlanTable] ([OlympiadPlanId], [OlympiadId], [Date]) VALUES (15, 1049, CAST(N'2021-04-24' AS Date))
INSERT [dbo].[OlympiadPlanTable] ([OlympiadPlanId], [OlympiadId], [Date]) VALUES (16, 16, CAST(N'2021-03-15' AS Date))
INSERT [dbo].[OlympiadPlanTable] ([OlympiadPlanId], [OlympiadId], [Date]) VALUES (17, 17, CAST(N'2021-03-25' AS Date))
INSERT [dbo].[OlympiadPlanTable] ([OlympiadPlanId], [OlympiadId], [Date]) VALUES (18, 18, CAST(N'2021-03-15' AS Date))
INSERT [dbo].[OlympiadPlanTable] ([OlympiadPlanId], [OlympiadId], [Date]) VALUES (19, 19, CAST(N'2021-03-25' AS Date))
INSERT [dbo].[OlympiadPlanTable] ([OlympiadPlanId], [OlympiadId], [Date]) VALUES (21, 20, CAST(N'2021-05-12' AS Date))
INSERT [dbo].[OlympiadPlanTable] ([OlympiadPlanId], [OlympiadId], [Date]) VALUES (22, 22, CAST(N'2021-03-15' AS Date))
INSERT [dbo].[OlympiadPlanTable] ([OlympiadPlanId], [OlympiadId], [Date]) VALUES (23, 23, CAST(N'2021-03-25' AS Date))
INSERT [dbo].[OlympiadPlanTable] ([OlympiadPlanId], [OlympiadId], [Date]) VALUES (24, 24, CAST(N'2021-03-15' AS Date))
INSERT [dbo].[OlympiadPlanTable] ([OlympiadPlanId], [OlympiadId], [Date]) VALUES (25, 25, CAST(N'2021-03-25' AS Date))
INSERT [dbo].[OlympiadPlanTable] ([OlympiadPlanId], [OlympiadId], [Date]) VALUES (27, 26, CAST(N'2021-05-12' AS Date))
INSERT [dbo].[OlympiadPlanTable] ([OlympiadPlanId], [OlympiadId], [Date]) VALUES (28, 28, CAST(N'2021-05-30' AS Date))
INSERT [dbo].[OlympiadPlanTable] ([OlympiadPlanId], [OlympiadId], [Date]) VALUES (1041, 14, CAST(N'2021-05-12' AS Date))
INSERT [dbo].[OlympiadPlanTable] ([OlympiadPlanId], [OlympiadId], [Date]) VALUES (1042, 1046, CAST(N'2021-04-01' AS Date))
INSERT [dbo].[OlympiadPlanTable] ([OlympiadPlanId], [OlympiadId], [Date]) VALUES (1043, 1047, CAST(N'2021-04-20' AS Date))
INSERT [dbo].[OlympiadPlanTable] ([OlympiadPlanId], [OlympiadId], [Date]) VALUES (1044, 28, CAST(N'2021-05-10' AS Date))
INSERT [dbo].[OlympiadPlanTable] ([OlympiadPlanId], [OlympiadId], [Date]) VALUES (1045, 1050, CAST(N'2021-05-03' AS Date))
INSERT [dbo].[OlympiadPlanTable] ([OlympiadPlanId], [OlympiadId], [Date]) VALUES (1046, 1051, CAST(N'2021-05-25' AS Date))
INSERT [dbo].[OlympiadPlanTable] ([OlympiadPlanId], [OlympiadId], [Date]) VALUES (1048, 12, CAST(N'2021-03-15' AS Date))
INSERT [dbo].[OlympiadPlanTable] ([OlympiadPlanId], [OlympiadId], [Date]) VALUES (1049, 10, CAST(N'2021-03-15' AS Date))
INSERT [dbo].[OlympiadPlanTable] ([OlympiadPlanId], [OlympiadId], [Date]) VALUES (1050, 15, CAST(N'2021-05-30' AS Date))
INSERT [dbo].[OlympiadPlanTable] ([OlympiadPlanId], [OlympiadId], [Date]) VALUES (1051, 21, CAST(N'2021-05-30' AS Date))
INSERT [dbo].[OlympiadPlanTable] ([OlympiadPlanId], [OlympiadId], [Date]) VALUES (1052, 27, CAST(N'2021-05-30' AS Date))
SET IDENTITY_INSERT [dbo].[OlympiadPlanTable] OFF
GO
SET IDENTITY_INSERT [dbo].[OlympiadTable] ON 

INSERT [dbo].[OlympiadTable] ([OlympiadId], [OlympiadName], [SubjectId], [Description], [Team]) VALUES (1, N'Математика -  царица наук
', 1, N'Цели и задачи олимпиады:
Выявление и развитие интеллектуальных, познавательных способностей, широты кругозора одаренных учащихся школ на всей територии Российской Федерации, выявления глубины их знаний.
', 0)
INSERT [dbo].[OlympiadTable] ([OlympiadId], [OlympiadName], [SubjectId], [Description], [Team]) VALUES (2, N'Русский медвежонок', 2, N'Цели и задачи: предоставление учащимся возможности соревноваться в масштабе, выходящем за рамки региона, не выезжая из него
', 0)
INSERT [dbo].[OlympiadTable] ([OlympiadId], [OlympiadName], [SubjectId], [Description], [Team]) VALUES (3, N'Английский бульдог', 3, N'Целями и задачами конкурса являются:
— развитие у учащихся интереса к английскому языку;
— активизация внеурочной работы;
', 0)
INSERT [dbo].[OlympiadTable] ([OlympiadId], [OlympiadName], [SubjectId], [Description], [Team]) VALUES (4, N'Всероссийская олимпиада по биологии', 4, N'Целями и задачами конкурса являются:
— развитие у учащихся интереса к биологии;
— предоставление участникам Конкурса возможности альтернативной оценки собственных знаний.
', 0)
INSERT [dbo].[OlympiadTable] ([OlympiadId], [OlympiadName], [SubjectId], [Description], [Team]) VALUES (5, N'Всероссийская олимпиада по географии', 5, N'Целями и задачами конкурса являются:
— развитие у учащихся интереса к географии;
—Выявление одаренных детей с целью приглашения на Международный форум одаренных детей.', 0)
INSERT [dbo].[OlympiadTable] ([OlympiadId], [OlympiadName], [SubjectId], [Description], [Team]) VALUES (6, N'Всероссийская олимпиада по истории', 6, N'Целями и задачами конкурса являются:
— развитие у учащихся интереса к истории;', 0)
INSERT [dbo].[OlympiadTable] ([OlympiadId], [OlympiadName], [SubjectId], [Description], [Team]) VALUES (8, N'Спортивные состязания', 7, N'Целями и задачами конкурса являются:
— развитие у учащихся интереса к спорту;
— рказвитие у учащихся работать в команде.', 1)
INSERT [dbo].[OlympiadTable] ([OlympiadId], [OlympiadName], [SubjectId], [Description], [Team]) VALUES (10, N'Спорт - залог здоровья', 7, N'Цель: развить спортивные навыки и умение работать в команде', 1)
INSERT [dbo].[OlympiadTable] ([OlympiadId], [OlympiadName], [SubjectId], [Description], [Team]) VALUES (11, N'Мир IT-технологий', 14, N'Формирование и развитие у обучающихся научных и технологических знаний', 0)
INSERT [dbo].[OlympiadTable] ([OlympiadId], [OlympiadName], [SubjectId], [Description], [Team]) VALUES (1011, N'Всероссийская олимпиада по математике', 1, N'Цели и задачи олимпиады:
Выявление и развитие интеллектуальных, познавательных способностей, широты кругозора одаренных учащихся школ на всей територии Российской Федерации, выявления глубины их знаний.
', 0)
INSERT [dbo].[OlympiadTable] ([OlympiadId], [OlympiadName], [SubjectId], [Description], [Team]) VALUES (1014, N'Ломоносов', 1014, N'Проведение олимпиады позволяет выявить тех ее участников, которые имеют ярко выраженные творческие способности, серьезно интересуются русской словесностью, хорошо владеют нормами современного русского литературного языка.', 0)
SET IDENTITY_INSERT [dbo].[OlympiadTable] OFF
GO
SET IDENTITY_INSERT [dbo].[RegistrationOfParticipantsTable] ON 

INSERT [dbo].[RegistrationOfParticipantsTable] ([RegistrationOfParticipantsId], [StudentId], [Class], [Scores], [OlympiadPlanId], [ResultId], [RewardId], [Absent]) VALUES (2, 1, 3, CAST(20.00 AS Decimal(5, 2)), 1, 3, 1, 0)
INSERT [dbo].[RegistrationOfParticipantsTable] ([RegistrationOfParticipantsId], [StudentId], [Class], [Scores], [OlympiadPlanId], [ResultId], [RewardId], [Absent]) VALUES (3, 2, 7, CAST(15.00 AS Decimal(5, 2)), 2, 4, 1, 0)
INSERT [dbo].[RegistrationOfParticipantsTable] ([RegistrationOfParticipantsId], [StudentId], [Class], [Scores], [OlympiadPlanId], [ResultId], [RewardId], [Absent]) VALUES (5, 4, 3, CAST(25.00 AS Decimal(5, 2)), 4, 1, 3, 0)
INSERT [dbo].[RegistrationOfParticipantsTable] ([RegistrationOfParticipantsId], [StudentId], [Class], [Scores], [OlympiadPlanId], [ResultId], [RewardId], [Absent]) VALUES (6, 5, 6, CAST(15.00 AS Decimal(5, 2)), 5, 4, 1, 0)
INSERT [dbo].[RegistrationOfParticipantsTable] ([RegistrationOfParticipantsId], [StudentId], [Class], [Scores], [OlympiadPlanId], [ResultId], [RewardId], [Absent]) VALUES (9, 8, 6, CAST(15.00 AS Decimal(5, 2)), 8, 4, 1, 0)
INSERT [dbo].[RegistrationOfParticipantsTable] ([RegistrationOfParticipantsId], [StudentId], [Class], [Scores], [OlympiadPlanId], [ResultId], [RewardId], [Absent]) VALUES (15, 2, 7, CAST(60.00 AS Decimal(5, 2)), 22, 4, 1, 0)
INSERT [dbo].[RegistrationOfParticipantsTable] ([RegistrationOfParticipantsId], [StudentId], [Class], [Scores], [OlympiadPlanId], [ResultId], [RewardId], [Absent]) VALUES (22, 2, 7, CAST(100.00 AS Decimal(5, 2)), 16, 2, 1, 0)
INSERT [dbo].[RegistrationOfParticipantsTable] ([RegistrationOfParticipantsId], [StudentId], [Class], [Scores], [OlympiadPlanId], [ResultId], [RewardId], [Absent]) VALUES (24, 6, 10, CAST(99.00 AS Decimal(5, 2)), 24, 2, 1, 0)
INSERT [dbo].[RegistrationOfParticipantsTable] ([RegistrationOfParticipantsId], [StudentId], [Class], [Scores], [OlympiadPlanId], [ResultId], [RewardId], [Absent]) VALUES (25, 10, 11, CAST(70.00 AS Decimal(5, 2)), 18, 3, 1, 0)
INSERT [dbo].[RegistrationOfParticipantsTable] ([RegistrationOfParticipantsId], [StudentId], [Class], [Scores], [OlympiadPlanId], [ResultId], [RewardId], [Absent]) VALUES (1022, 9, 11, CAST(99.50 AS Decimal(5, 2)), 15, 2, 3, 0)
INSERT [dbo].[RegistrationOfParticipantsTable] ([RegistrationOfParticipantsId], [StudentId], [Class], [Scores], [OlympiadPlanId], [ResultId], [RewardId], [Absent]) VALUES (1023, 6, 10, CAST(92.00 AS Decimal(5, 2)), 15, 2, 3, 0)
INSERT [dbo].[RegistrationOfParticipantsTable] ([RegistrationOfParticipantsId], [StudentId], [Class], [Scores], [OlympiadPlanId], [ResultId], [RewardId], [Absent]) VALUES (1024, 14, 10, CAST(97.50 AS Decimal(5, 2)), 15, 2, 3, 0)
INSERT [dbo].[RegistrationOfParticipantsTable] ([RegistrationOfParticipantsId], [StudentId], [Class], [Scores], [OlympiadPlanId], [ResultId], [RewardId], [Absent]) VALUES (1025, 10, 11, CAST(75.00 AS Decimal(5, 2)), 15, 4, 1, 0)
INSERT [dbo].[RegistrationOfParticipantsTable] ([RegistrationOfParticipantsId], [StudentId], [Class], [Scores], [OlympiadPlanId], [ResultId], [RewardId], [Absent]) VALUES (1026, 13, 12, NULL, 15, NULL, NULL, NULL)
INSERT [dbo].[RegistrationOfParticipantsTable] ([RegistrationOfParticipantsId], [StudentId], [Class], [Scores], [OlympiadPlanId], [ResultId], [RewardId], [Absent]) VALUES (1027, 1015, 11, CAST(77.00 AS Decimal(5, 2)), 15, 4, 1, 0)
INSERT [dbo].[RegistrationOfParticipantsTable] ([RegistrationOfParticipantsId], [StudentId], [Class], [Scores], [OlympiadPlanId], [ResultId], [RewardId], [Absent]) VALUES (1028, 2, 7, CAST(100.00 AS Decimal(5, 2)), 1042, 1, 3, 0)
INSERT [dbo].[RegistrationOfParticipantsTable] ([RegistrationOfParticipantsId], [StudentId], [Class], [Scores], [OlympiadPlanId], [ResultId], [RewardId], [Absent]) VALUES (1029, 3, 9, CAST(35.00 AS Decimal(5, 2)), 1042, 8, 1, 0)
INSERT [dbo].[RegistrationOfParticipantsTable] ([RegistrationOfParticipantsId], [StudentId], [Class], [Scores], [OlympiadPlanId], [ResultId], [RewardId], [Absent]) VALUES (1030, 5, 6, CAST(50.00 AS Decimal(5, 2)), 1042, 8, 1, 0)
INSERT [dbo].[RegistrationOfParticipantsTable] ([RegistrationOfParticipantsId], [StudentId], [Class], [Scores], [OlympiadPlanId], [ResultId], [RewardId], [Absent]) VALUES (1031, 8, 6, CAST(80.00 AS Decimal(5, 2)), 1042, 3, 1, 0)
INSERT [dbo].[RegistrationOfParticipantsTable] ([RegistrationOfParticipantsId], [StudentId], [Class], [Scores], [OlympiadPlanId], [ResultId], [RewardId], [Absent]) VALUES (1032, 1016, 8, CAST(70.00 AS Decimal(5, 2)), 1042, 4, 1, 0)
INSERT [dbo].[RegistrationOfParticipantsTable] ([RegistrationOfParticipantsId], [StudentId], [Class], [Scores], [OlympiadPlanId], [ResultId], [RewardId], [Absent]) VALUES (1033, 1012, 5, CAST(85.00 AS Decimal(5, 2)), 1042, 3, 1, 0)
INSERT [dbo].[RegistrationOfParticipantsTable] ([RegistrationOfParticipantsId], [StudentId], [Class], [Scores], [OlympiadPlanId], [ResultId], [RewardId], [Absent]) VALUES (1034, 1014, 5, NULL, 1042, NULL, NULL, 1)
INSERT [dbo].[RegistrationOfParticipantsTable] ([RegistrationOfParticipantsId], [StudentId], [Class], [Scores], [OlympiadPlanId], [ResultId], [RewardId], [Absent]) VALUES (1038, 1017, 10, NULL, 15, NULL, NULL, NULL)
INSERT [dbo].[RegistrationOfParticipantsTable] ([RegistrationOfParticipantsId], [StudentId], [Class], [Scores], [OlympiadPlanId], [ResultId], [RewardId], [Absent]) VALUES (1044, 2, 7, CAST(50.00 AS Decimal(5, 2)), 1049, 8, 7, 0)
INSERT [dbo].[RegistrationOfParticipantsTable] ([RegistrationOfParticipantsId], [StudentId], [Class], [Scores], [OlympiadPlanId], [ResultId], [RewardId], [Absent]) VALUES (1045, 3, 9, CAST(61.00 AS Decimal(5, 2)), 1049, 4, 1, 0)
INSERT [dbo].[RegistrationOfParticipantsTable] ([RegistrationOfParticipantsId], [StudentId], [Class], [Scores], [OlympiadPlanId], [ResultId], [RewardId], [Absent]) VALUES (1046, 5, 6, CAST(98.50 AS Decimal(5, 2)), 16, 2, 1, 0)
INSERT [dbo].[RegistrationOfParticipantsTable] ([RegistrationOfParticipantsId], [StudentId], [Class], [Scores], [OlympiadPlanId], [ResultId], [RewardId], [Absent]) VALUES (1047, 1018, 7, CAST(79.50 AS Decimal(5, 2)), 16, 3, 1, 0)
INSERT [dbo].[RegistrationOfParticipantsTable] ([RegistrationOfParticipantsId], [StudentId], [Class], [Scores], [OlympiadPlanId], [ResultId], [RewardId], [Absent]) VALUES (1048, 1012, 5, CAST(65.00 AS Decimal(5, 2)), 22, 4, 1, 0)
INSERT [dbo].[RegistrationOfParticipantsTable] ([RegistrationOfParticipantsId], [StudentId], [Class], [Scores], [OlympiadPlanId], [ResultId], [RewardId], [Absent]) VALUES (1049, 1016, 8, CAST(88.00 AS Decimal(5, 2)), 22, 3, 1, 0)
INSERT [dbo].[RegistrationOfParticipantsTable] ([RegistrationOfParticipantsId], [StudentId], [Class], [Scores], [OlympiadPlanId], [ResultId], [RewardId], [Absent]) VALUES (1050, 2, 7, CAST(99.00 AS Decimal(5, 2)), 11, 2, 1, 0)
INSERT [dbo].[RegistrationOfParticipantsTable] ([RegistrationOfParticipantsId], [StudentId], [Class], [Scores], [OlympiadPlanId], [ResultId], [RewardId], [Absent]) VALUES (1051, 5, 6, CAST(45.00 AS Decimal(5, 2)), 17, 8, 7, 0)
INSERT [dbo].[RegistrationOfParticipantsTable] ([RegistrationOfParticipantsId], [StudentId], [Class], [Scores], [OlympiadPlanId], [ResultId], [RewardId], [Absent]) VALUES (1052, 1016, 8, CAST(72.00 AS Decimal(5, 2)), 23, 3, 1, 0)
INSERT [dbo].[RegistrationOfParticipantsTable] ([RegistrationOfParticipantsId], [StudentId], [Class], [Scores], [OlympiadPlanId], [ResultId], [RewardId], [Absent]) VALUES (1053, 6, 10, CAST(72.00 AS Decimal(5, 2)), 1048, 3, 1, 0)
INSERT [dbo].[RegistrationOfParticipantsTable] ([RegistrationOfParticipantsId], [StudentId], [Class], [Scores], [OlympiadPlanId], [ResultId], [RewardId], [Absent]) VALUES (1054, 1019, 10, CAST(83.00 AS Decimal(5, 2)), 1048, 3, 1, 0)
INSERT [dbo].[RegistrationOfParticipantsTable] ([RegistrationOfParticipantsId], [StudentId], [Class], [Scores], [OlympiadPlanId], [ResultId], [RewardId], [Absent]) VALUES (1055, 1019, 10, CAST(50.00 AS Decimal(5, 2)), 13, 8, 7, 0)
INSERT [dbo].[RegistrationOfParticipantsTable] ([RegistrationOfParticipantsId], [StudentId], [Class], [Scores], [OlympiadPlanId], [ResultId], [RewardId], [Absent]) VALUES (1056, 1019, 10, NULL, 1041, NULL, NULL, NULL)
INSERT [dbo].[RegistrationOfParticipantsTable] ([RegistrationOfParticipantsId], [StudentId], [Class], [Scores], [OlympiadPlanId], [ResultId], [RewardId], [Absent]) VALUES (1057, 1021, 12, CAST(58.00 AS Decimal(5, 2)), 18, 8, 7, 0)
INSERT [dbo].[RegistrationOfParticipantsTable] ([RegistrationOfParticipantsId], [StudentId], [Class], [Scores], [OlympiadPlanId], [ResultId], [RewardId], [Absent]) VALUES (1058, 1020, 12, CAST(60.50 AS Decimal(5, 2)), 18, 4, 1, 0)
INSERT [dbo].[RegistrationOfParticipantsTable] ([RegistrationOfParticipantsId], [StudentId], [Class], [Scores], [OlympiadPlanId], [ResultId], [RewardId], [Absent]) VALUES (1059, 9, 11, CAST(100.00 AS Decimal(5, 2)), 18, 2, 1, 0)
INSERT [dbo].[RegistrationOfParticipantsTable] ([RegistrationOfParticipantsId], [StudentId], [Class], [Scores], [OlympiadPlanId], [ResultId], [RewardId], [Absent]) VALUES (1060, 1020, 12, CAST(100.00 AS Decimal(5, 2)), 19, 2, 1, 0)
INSERT [dbo].[RegistrationOfParticipantsTable] ([RegistrationOfParticipantsId], [StudentId], [Class], [Scores], [OlympiadPlanId], [ResultId], [RewardId], [Absent]) VALUES (1061, 9, 11, CAST(99.00 AS Decimal(5, 2)), 19, 2, 1, 0)
INSERT [dbo].[RegistrationOfParticipantsTable] ([RegistrationOfParticipantsId], [StudentId], [Class], [Scores], [OlympiadPlanId], [ResultId], [RewardId], [Absent]) VALUES (1063, 9, 11, NULL, 21, NULL, NULL, NULL)
INSERT [dbo].[RegistrationOfParticipantsTable] ([RegistrationOfParticipantsId], [StudentId], [Class], [Scores], [OlympiadPlanId], [ResultId], [RewardId], [Absent]) VALUES (1064, 14, 10, CAST(86.50 AS Decimal(5, 2)), 24, 3, 1, 0)
INSERT [dbo].[RegistrationOfParticipantsTable] ([RegistrationOfParticipantsId], [StudentId], [Class], [Scores], [OlympiadPlanId], [ResultId], [RewardId], [Absent]) VALUES (1065, 1017, 10, CAST(94.50 AS Decimal(5, 2)), 24, 2, 1, 0)
INSERT [dbo].[RegistrationOfParticipantsTable] ([RegistrationOfParticipantsId], [StudentId], [Class], [Scores], [OlympiadPlanId], [ResultId], [RewardId], [Absent]) VALUES (1066, 6, 10, CAST(96.50 AS Decimal(5, 2)), 25, 2, 1, 0)
INSERT [dbo].[RegistrationOfParticipantsTable] ([RegistrationOfParticipantsId], [StudentId], [Class], [Scores], [OlympiadPlanId], [ResultId], [RewardId], [Absent]) VALUES (1067, 6, 10, NULL, 27, NULL, NULL, NULL)
INSERT [dbo].[RegistrationOfParticipantsTable] ([RegistrationOfParticipantsId], [StudentId], [Class], [Scores], [OlympiadPlanId], [ResultId], [RewardId], [Absent]) VALUES (1068, 1013, 4, CAST(19.00 AS Decimal(5, 2)), 7, 3, 1, 0)
INSERT [dbo].[RegistrationOfParticipantsTable] ([RegistrationOfParticipantsId], [StudentId], [Class], [Scores], [OlympiadPlanId], [ResultId], [RewardId], [Absent]) VALUES (1069, 11, 2, CAST(19.50 AS Decimal(5, 2)), 7, 3, 1, 0)
INSERT [dbo].[RegistrationOfParticipantsTable] ([RegistrationOfParticipantsId], [StudentId], [Class], [Scores], [OlympiadPlanId], [ResultId], [RewardId], [Absent]) VALUES (1070, 22, 1, CAST(25.00 AS Decimal(5, 2)), 4, 1, 3, 0)
INSERT [dbo].[RegistrationOfParticipantsTable] ([RegistrationOfParticipantsId], [StudentId], [Class], [Scores], [OlympiadPlanId], [ResultId], [RewardId], [Absent]) VALUES (1071, 12, 3, CAST(15.00 AS Decimal(5, 2)), 1, 4, 1, 0)
INSERT [dbo].[RegistrationOfParticipantsTable] ([RegistrationOfParticipantsId], [StudentId], [Class], [Scores], [OlympiadPlanId], [ResultId], [RewardId], [Absent]) VALUES (1072, 7, 2, CAST(17.00 AS Decimal(5, 2)), 1, 4, 1, 0)
INSERT [dbo].[RegistrationOfParticipantsTable] ([RegistrationOfParticipantsId], [StudentId], [Class], [Scores], [OlympiadPlanId], [ResultId], [RewardId], [Absent]) VALUES (1073, 1014, 5, CAST(25.00 AS Decimal(5, 2)), 8, 1, 3, 0)
INSERT [dbo].[RegistrationOfParticipantsTable] ([RegistrationOfParticipantsId], [StudentId], [Class], [Scores], [OlympiadPlanId], [ResultId], [RewardId], [Absent]) VALUES (1074, 1016, 8, NULL, 5, NULL, NULL, 1)
INSERT [dbo].[RegistrationOfParticipantsTable] ([RegistrationOfParticipantsId], [StudentId], [Class], [Scores], [OlympiadPlanId], [ResultId], [RewardId], [Absent]) VALUES (1075, 1012, 5, CAST(10.00 AS Decimal(5, 2)), 2, 8, 7, 0)
INSERT [dbo].[RegistrationOfParticipantsTable] ([RegistrationOfParticipantsId], [StudentId], [Class], [Scores], [OlympiadPlanId], [ResultId], [RewardId], [Absent]) VALUES (1076, 8, 6, NULL, 1045, NULL, NULL, NULL)
INSERT [dbo].[RegistrationOfParticipantsTable] ([RegistrationOfParticipantsId], [StudentId], [Class], [Scores], [OlympiadPlanId], [ResultId], [RewardId], [Absent]) VALUES (1077, 1016, 8, NULL, 1045, NULL, NULL, NULL)
INSERT [dbo].[RegistrationOfParticipantsTable] ([RegistrationOfParticipantsId], [StudentId], [Class], [Scores], [OlympiadPlanId], [ResultId], [RewardId], [Absent]) VALUES (1078, 1012, 5, NULL, 1045, NULL, NULL, NULL)
INSERT [dbo].[RegistrationOfParticipantsTable] ([RegistrationOfParticipantsId], [StudentId], [Class], [Scores], [OlympiadPlanId], [ResultId], [RewardId], [Absent]) VALUES (1079, 3, 9, NULL, 1045, NULL, NULL, NULL)
INSERT [dbo].[RegistrationOfParticipantsTable] ([RegistrationOfParticipantsId], [StudentId], [Class], [Scores], [OlympiadPlanId], [ResultId], [RewardId], [Absent]) VALUES (1080, 1023, 8, NULL, 1044, NULL, NULL, NULL)
INSERT [dbo].[RegistrationOfParticipantsTable] ([RegistrationOfParticipantsId], [StudentId], [Class], [Scores], [OlympiadPlanId], [ResultId], [RewardId], [Absent]) VALUES (1081, 1024, 7, NULL, 1044, NULL, NULL, NULL)
INSERT [dbo].[RegistrationOfParticipantsTable] ([RegistrationOfParticipantsId], [StudentId], [Class], [Scores], [OlympiadPlanId], [ResultId], [RewardId], [Absent]) VALUES (1082, 1018, 7, NULL, 1044, NULL, NULL, NULL)
INSERT [dbo].[RegistrationOfParticipantsTable] ([RegistrationOfParticipantsId], [StudentId], [Class], [Scores], [OlympiadPlanId], [ResultId], [RewardId], [Absent]) VALUES (1083, 2, 7, NULL, 1044, NULL, NULL, NULL)
INSERT [dbo].[RegistrationOfParticipantsTable] ([RegistrationOfParticipantsId], [StudentId], [Class], [Scores], [OlympiadPlanId], [ResultId], [RewardId], [Absent]) VALUES (1084, 1012, 5, CAST(90.00 AS Decimal(5, 2)), 1043, 2, 1, 0)
INSERT [dbo].[RegistrationOfParticipantsTable] ([RegistrationOfParticipantsId], [StudentId], [Class], [Scores], [OlympiadPlanId], [ResultId], [RewardId], [Absent]) VALUES (1085, 3, 9, CAST(89.00 AS Decimal(5, 2)), 1043, 3, 1, 0)
INSERT [dbo].[RegistrationOfParticipantsTable] ([RegistrationOfParticipantsId], [StudentId], [Class], [Scores], [OlympiadPlanId], [ResultId], [RewardId], [Absent]) VALUES (1086, 8, 6, CAST(91.50 AS Decimal(5, 2)), 1043, 2, 1, 0)
INSERT [dbo].[RegistrationOfParticipantsTable] ([RegistrationOfParticipantsId], [StudentId], [Class], [Scores], [OlympiadPlanId], [ResultId], [RewardId], [Absent]) VALUES (1087, 1016, 8, CAST(95.00 AS Decimal(5, 2)), 1043, 2, 1, 0)
SET IDENTITY_INSERT [dbo].[RegistrationOfParticipantsTable] OFF
GO
SET IDENTITY_INSERT [dbo].[ResultTable] ON 

INSERT [dbo].[ResultTable] ([ResultId], [Result]) VALUES (1, N'Гран-при')
INSERT [dbo].[ResultTable] ([ResultId], [Result]) VALUES (2, N'1 место')
INSERT [dbo].[ResultTable] ([ResultId], [Result]) VALUES (3, N'2 место')
INSERT [dbo].[ResultTable] ([ResultId], [Result]) VALUES (4, N'3 место')
INSERT [dbo].[ResultTable] ([ResultId], [Result]) VALUES (5, N'1 степень')
INSERT [dbo].[ResultTable] ([ResultId], [Result]) VALUES (6, N'2 степень')
INSERT [dbo].[ResultTable] ([ResultId], [Result]) VALUES (7, N'3 степень')
INSERT [dbo].[ResultTable] ([ResultId], [Result]) VALUES (8, N'Благодарность за участие')
SET IDENTITY_INSERT [dbo].[ResultTable] OFF
GO
SET IDENTITY_INSERT [dbo].[RewardTable] ON 

INSERT [dbo].[RewardTable] ([RewardId], [Reward]) VALUES (1, N'Грамота
')
INSERT [dbo].[RewardTable] ([RewardId], [Reward]) VALUES (2, N'Сертификат
')
INSERT [dbo].[RewardTable] ([RewardId], [Reward]) VALUES (3, N'Диплом
')
INSERT [dbo].[RewardTable] ([RewardId], [Reward]) VALUES (4, N'Кубок
')
INSERT [dbo].[RewardTable] ([RewardId], [Reward]) VALUES (5, N'Медаль
')
INSERT [dbo].[RewardTable] ([RewardId], [Reward]) VALUES (6, N'Приз
')
INSERT [dbo].[RewardTable] ([RewardId], [Reward]) VALUES (7, N'Благодарственное письмо
')
INSERT [dbo].[RewardTable] ([RewardId], [Reward]) VALUES (8, N'Денежное вознаграждение')
SET IDENTITY_INSERT [dbo].[RewardTable] OFF
GO
SET IDENTITY_INSERT [dbo].[SchoolStageTable] ON 

INSERT [dbo].[SchoolStageTable] ([SchoolStageId], [SchoolStage]) VALUES (1, N'Начальные классы')
INSERT [dbo].[SchoolStageTable] ([SchoolStageId], [SchoolStage]) VALUES (2, N'Средние классы')
INSERT [dbo].[SchoolStageTable] ([SchoolStageId], [SchoolStage]) VALUES (3, N'Старшие классы')
SET IDENTITY_INSERT [dbo].[SchoolStageTable] OFF
GO
SET IDENTITY_INSERT [dbo].[StudentTable] ON 

INSERT [dbo].[StudentTable] ([StudentId], [StudentSurname], [StudentName], [StudentMiddle], [StudentClass]) VALUES (1, N'Таныгина', N'Кристина', N'Игоревна', 3)
INSERT [dbo].[StudentTable] ([StudentId], [StudentSurname], [StudentName], [StudentMiddle], [StudentClass]) VALUES (2, N'Афанасьев', N'Дмитрий', N'Александрович', 7)
INSERT [dbo].[StudentTable] ([StudentId], [StudentSurname], [StudentName], [StudentMiddle], [StudentClass]) VALUES (3, N'Букенов', N'Владислав', N'Олегович', 9)
INSERT [dbo].[StudentTable] ([StudentId], [StudentSurname], [StudentName], [StudentMiddle], [StudentClass]) VALUES (4, N'Скоморохина', N'Анжела', N'Алексеевна', 3)
INSERT [dbo].[StudentTable] ([StudentId], [StudentSurname], [StudentName], [StudentMiddle], [StudentClass]) VALUES (5, N'Ветошкина', N'Наталья', N'Анатольевна', 6)
INSERT [dbo].[StudentTable] ([StudentId], [StudentSurname], [StudentName], [StudentMiddle], [StudentClass]) VALUES (6, N'Василенко
', N'Даниил', N'Александрович', 10)
INSERT [dbo].[StudentTable] ([StudentId], [StudentSurname], [StudentName], [StudentMiddle], [StudentClass]) VALUES (7, N'Фадеев', N'Матвей', N'Михайлович', 2)
INSERT [dbo].[StudentTable] ([StudentId], [StudentSurname], [StudentName], [StudentMiddle], [StudentClass]) VALUES (8, N'Митрофанов
', N'Тимофей
', N'Николаевич', 6)
INSERT [dbo].[StudentTable] ([StudentId], [StudentSurname], [StudentName], [StudentMiddle], [StudentClass]) VALUES (9, N'Мартыновский
', N'Денис', N'Александрович
', 11)
INSERT [dbo].[StudentTable] ([StudentId], [StudentSurname], [StudentName], [StudentMiddle], [StudentClass]) VALUES (10, N'Тиунова', N'Алина', N'Михайловна', 11)
INSERT [dbo].[StudentTable] ([StudentId], [StudentSurname], [StudentName], [StudentMiddle], [StudentClass]) VALUES (11, N'Красножон', N'Елена', N'Викторовна', 2)
INSERT [dbo].[StudentTable] ([StudentId], [StudentSurname], [StudentName], [StudentMiddle], [StudentClass]) VALUES (12, N'Усольцева', N'Екатерина', N'Михайловна', 3)
INSERT [dbo].[StudentTable] ([StudentId], [StudentSurname], [StudentName], [StudentMiddle], [StudentClass]) VALUES (13, N'Медведева', N'Евгения', N'Евгеньевна', 12)
INSERT [dbo].[StudentTable] ([StudentId], [StudentSurname], [StudentName], [StudentMiddle], [StudentClass]) VALUES (14, N'Устенко', N'Марина', N'Сергеевна', 10)
INSERT [dbo].[StudentTable] ([StudentId], [StudentSurname], [StudentName], [StudentMiddle], [StudentClass]) VALUES (22, N'Михеева', N'Вероника', N'Антоновна', 1)
INSERT [dbo].[StudentTable] ([StudentId], [StudentSurname], [StudentName], [StudentMiddle], [StudentClass]) VALUES (23, N'Смоленко', N'Геннадий', N'Андреевич', 4)
INSERT [dbo].[StudentTable] ([StudentId], [StudentSurname], [StudentName], [StudentMiddle], [StudentClass]) VALUES (1011, N'Сотников', N'Александр', N'Александрович', 1)
INSERT [dbo].[StudentTable] ([StudentId], [StudentSurname], [StudentName], [StudentMiddle], [StudentClass]) VALUES (1012, N'Кухаренко', N'Алена', N'Леонидовна', 5)
INSERT [dbo].[StudentTable] ([StudentId], [StudentSurname], [StudentName], [StudentMiddle], [StudentClass]) VALUES (1013, N'Арсеньев', N'Виктор', N'Николаевич', 4)
INSERT [dbo].[StudentTable] ([StudentId], [StudentSurname], [StudentName], [StudentMiddle], [StudentClass]) VALUES (1014, N'Юдина', N'Галина', N'Александровна', 5)
INSERT [dbo].[StudentTable] ([StudentId], [StudentSurname], [StudentName], [StudentMiddle], [StudentClass]) VALUES (1015, N'Ярков', N'Сергей', N'Яковлевич', 11)
INSERT [dbo].[StudentTable] ([StudentId], [StudentSurname], [StudentName], [StudentMiddle], [StudentClass]) VALUES (1016, N'Ломиковская', N'Ирина', N'Михайловна', 8)
INSERT [dbo].[StudentTable] ([StudentId], [StudentSurname], [StudentName], [StudentMiddle], [StudentClass]) VALUES (1017, N'Хофман', N'Ксения', N'Альбертовна', 10)
INSERT [dbo].[StudentTable] ([StudentId], [StudentSurname], [StudentName], [StudentMiddle], [StudentClass]) VALUES (1018, N'Карпов', N'Владимир', N'Антонович', 7)
INSERT [dbo].[StudentTable] ([StudentId], [StudentSurname], [StudentName], [StudentMiddle], [StudentClass]) VALUES (1019, N'Ермоленко', N'Андрей', N'Евгеньевич', 10)
INSERT [dbo].[StudentTable] ([StudentId], [StudentSurname], [StudentName], [StudentMiddle], [StudentClass]) VALUES (1020, N'Игнатьева', N'Вероника', N'Александровна', 12)
INSERT [dbo].[StudentTable] ([StudentId], [StudentSurname], [StudentName], [StudentMiddle], [StudentClass]) VALUES (1021, N'Зуева', N'Марина', N'Алексеевна', 12)
INSERT [dbo].[StudentTable] ([StudentId], [StudentSurname], [StudentName], [StudentMiddle], [StudentClass]) VALUES (1022, N'Нурсинова', N'Алия', N'Муратовна', 10)
INSERT [dbo].[StudentTable] ([StudentId], [StudentSurname], [StudentName], [StudentMiddle], [StudentClass]) VALUES (1023, N'Иванов', N'Алексей', N'Александрович', 8)
INSERT [dbo].[StudentTable] ([StudentId], [StudentSurname], [StudentName], [StudentMiddle], [StudentClass]) VALUES (1024, N'Матюх', N'Дмитрий', N'Николаевич', 7)
INSERT [dbo].[StudentTable] ([StudentId], [StudentSurname], [StudentName], [StudentMiddle], [StudentClass]) VALUES (1025, N'Кокош', N'Дарья', N'Юрьевна', 9)
INSERT [dbo].[StudentTable] ([StudentId], [StudentSurname], [StudentName], [StudentMiddle], [StudentClass]) VALUES (1027, N'Тачитдинова', N'Наталья', N'Рамильевна', 8)
SET IDENTITY_INSERT [dbo].[StudentTable] OFF
GO
SET IDENTITY_INSERT [dbo].[SubjectTable] ON 

INSERT [dbo].[SubjectTable] ([SubjectId], [Subject]) VALUES (1, N'Математика')
INSERT [dbo].[SubjectTable] ([SubjectId], [Subject]) VALUES (2, N'Русский язык')
INSERT [dbo].[SubjectTable] ([SubjectId], [Subject]) VALUES (3, N'Английский')
INSERT [dbo].[SubjectTable] ([SubjectId], [Subject]) VALUES (4, N'Биология')
INSERT [dbo].[SubjectTable] ([SubjectId], [Subject]) VALUES (5, N'География')
INSERT [dbo].[SubjectTable] ([SubjectId], [Subject]) VALUES (6, N'История')
INSERT [dbo].[SubjectTable] ([SubjectId], [Subject]) VALUES (7, N'Физическая культура')
INSERT [dbo].[SubjectTable] ([SubjectId], [Subject]) VALUES (8, N'Геометрия')
INSERT [dbo].[SubjectTable] ([SubjectId], [Subject]) VALUES (9, N'Алгебра')
INSERT [dbo].[SubjectTable] ([SubjectId], [Subject]) VALUES (10, N'Обществознание')
INSERT [dbo].[SubjectTable] ([SubjectId], [Subject]) VALUES (11, N'Технология')
INSERT [dbo].[SubjectTable] ([SubjectId], [Subject]) VALUES (12, N'Чтение')
INSERT [dbo].[SubjectTable] ([SubjectId], [Subject]) VALUES (13, N'Самопознание')
INSERT [dbo].[SubjectTable] ([SubjectId], [Subject]) VALUES (14, N'Информатика')
INSERT [dbo].[SubjectTable] ([SubjectId], [Subject]) VALUES (1014, N'Литература')
SET IDENTITY_INSERT [dbo].[SubjectTable] OFF
GO
ALTER TABLE [dbo].[OlimpiadByAgeAndLevelTable]  WITH CHECK ADD  CONSTRAINT [FK_OlimpiadByAgeAndLevelTable_OlympiadByAgeTable] FOREIGN KEY([OlympiadByAgeId])
REFERENCES [dbo].[OlympiadByAgeTable] ([OlympiadByAgeId])
GO
ALTER TABLE [dbo].[OlimpiadByAgeAndLevelTable] CHECK CONSTRAINT [FK_OlimpiadByAgeAndLevelTable_OlympiadByAgeTable]
GO
ALTER TABLE [dbo].[OlimpiadByAgeAndLevelTable]  WITH CHECK ADD  CONSTRAINT [FK_OlimpiadByAgeAndLevelTable_OlympiadLevelTable] FOREIGN KEY([OlympiadLevelId])
REFERENCES [dbo].[LevelTable] ([OlympiadLevelId])
GO
ALTER TABLE [dbo].[OlimpiadByAgeAndLevelTable] CHECK CONSTRAINT [FK_OlimpiadByAgeAndLevelTable_OlympiadLevelTable]
GO
ALTER TABLE [dbo].[OlympiadByAgeTable]  WITH CHECK ADD  CONSTRAINT [FK_OlympiadByAgeTable_OlympiadTable] FOREIGN KEY([OlympiadId])
REFERENCES [dbo].[OlympiadTable] ([OlympiadId])
GO
ALTER TABLE [dbo].[OlympiadByAgeTable] CHECK CONSTRAINT [FK_OlympiadByAgeTable_OlympiadTable]
GO
ALTER TABLE [dbo].[OlympiadByAgeTable]  WITH CHECK ADD  CONSTRAINT [FK_OlympiadByAgeTable_SchoolStageTable] FOREIGN KEY([SchoolStageId])
REFERENCES [dbo].[SchoolStageTable] ([SchoolStageId])
GO
ALTER TABLE [dbo].[OlympiadByAgeTable] CHECK CONSTRAINT [FK_OlympiadByAgeTable_SchoolStageTable]
GO
ALTER TABLE [dbo].[OlympiadPlanTable]  WITH CHECK ADD  CONSTRAINT [FK_OlympiadPlanTable_OlimpiadByAgeAndLevelTable] FOREIGN KEY([OlympiadId])
REFERENCES [dbo].[OlimpiadByAgeAndLevelTable] ([Id])
GO
ALTER TABLE [dbo].[OlympiadPlanTable] CHECK CONSTRAINT [FK_OlympiadPlanTable_OlimpiadByAgeAndLevelTable]
GO
ALTER TABLE [dbo].[OlympiadTable]  WITH CHECK ADD  CONSTRAINT [FK_OlympiadTable_SubjectTable] FOREIGN KEY([SubjectId])
REFERENCES [dbo].[SubjectTable] ([SubjectId])
GO
ALTER TABLE [dbo].[OlympiadTable] CHECK CONSTRAINT [FK_OlympiadTable_SubjectTable]
GO
ALTER TABLE [dbo].[RegistrationOfParticipantsTable]  WITH CHECK ADD  CONSTRAINT [FK_RegistrationOfParticipantsTable_OlympiadPlanTable] FOREIGN KEY([OlympiadPlanId])
REFERENCES [dbo].[OlympiadPlanTable] ([OlympiadPlanId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[RegistrationOfParticipantsTable] CHECK CONSTRAINT [FK_RegistrationOfParticipantsTable_OlympiadPlanTable]
GO
ALTER TABLE [dbo].[RegistrationOfParticipantsTable]  WITH CHECK ADD  CONSTRAINT [FK_RegistrationOfParticipantsTable_ResultTable] FOREIGN KEY([ResultId])
REFERENCES [dbo].[ResultTable] ([ResultId])
GO
ALTER TABLE [dbo].[RegistrationOfParticipantsTable] CHECK CONSTRAINT [FK_RegistrationOfParticipantsTable_ResultTable]
GO
ALTER TABLE [dbo].[RegistrationOfParticipantsTable]  WITH CHECK ADD  CONSTRAINT [FK_RegistrationOfParticipantsTable_RewardTable] FOREIGN KEY([RewardId])
REFERENCES [dbo].[RewardTable] ([RewardId])
GO
ALTER TABLE [dbo].[RegistrationOfParticipantsTable] CHECK CONSTRAINT [FK_RegistrationOfParticipantsTable_RewardTable]
GO
ALTER TABLE [dbo].[RegistrationOfParticipantsTable]  WITH CHECK ADD  CONSTRAINT [FK_RegistrationOfParticipantsTable_StudentTable] FOREIGN KEY([StudentId])
REFERENCES [dbo].[StudentTable] ([StudentId])
GO
ALTER TABLE [dbo].[RegistrationOfParticipantsTable] CHECK CONSTRAINT [FK_RegistrationOfParticipantsTable_StudentTable]
GO
ALTER TABLE [dbo].[StudentTable]  WITH CHECK ADD  CONSTRAINT [FK_StudentTable_ClassTable] FOREIGN KEY([StudentClass])
REFERENCES [dbo].[ClassTable] ([ClassId])
GO
ALTER TABLE [dbo].[StudentTable] CHECK CONSTRAINT [FK_StudentTable_ClassTable]
GO
USE [master]
GO
ALTER DATABASE [dbRegistrationOfParticipationInOlympiads] SET  READ_WRITE 
GO
